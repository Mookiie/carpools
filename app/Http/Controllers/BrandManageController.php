<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BrandManageController extends Controller
{
    //

         public function AddBrand(Request $req)
         {
           $emp_id =$req->input("id");
           return view('Models.Brand.addbrand',['emp_id'=>$emp_id]);
         }

         public function AddBrandDB(Request $req)
         {
           date_default_timezone_set("Asia/Bangkok");
           $emp_id = strtoupper($req->input("emp_id")) ;
           $brand_id=  $req->input("brand_id");
           $brand_name = $req->input("brand_name");
           $date =date("Y-m-d H:i:s");
           $msg = array();

                    $sql = DB::table('tb_brand')
                                 ->insert(['brand_id' => $brand_id,
                                           'brand_name' => $brand_name,
                                           'update_by' =>$emp_id,
                                           'update_date'=>$date]);
                   if(!$sql){
                         $msg = array("type"=>"","success"=>false,"msg"=>"");

                   }else{
                       $msg = array("type"=>"","success"=>true,"msg"=>"");
                   }
                   return Response(json_encode($msg));
           }

           public function EditBrand(Request $req)
           {
             $emp_id =$req->input("emp_id");
             $brand_id =$req->input("brand");
             return view('Models.Brand.editbrand',['emp_id'=>$emp_id,'brand_id'=>$brand_id]);
           }

           public function UpdateBrandDB(Request $req)
           {
             date_default_timezone_set("Asia/Bangkok");
             $emp_id = strtoupper($req->input("emp_id")) ;
             $brand_id=  $req->input("brand_id");
             $brand_name = $req->input("brand_name");
             $date =date("Y-m-d H:i:s");
             $msg = array();

                      $sql = DB::table('tb_brand')
                              ->where('brand_id', '=' ,$brand_id)
                              ->update(['brand_name' => $brand_name]);

                      $msg = array("type"=>"","success"=>true,"msg"=>"");

                     return Response(json_encode($msg));
             }
}
