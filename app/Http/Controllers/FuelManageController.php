<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FuelManageController extends Controller
{
    //

       public function AddFuel(Request $req)
       {
         $emp_id =$req->input("id");
         return view('Models.Fuel.addfuel',['emp_id'=>$emp_id]);
       }

       public function AddFuelDB(Request $req)
       {
         date_default_timezone_set("Asia/Bangkok");
         $emp_id = strtoupper($req->input("emp_id")) ;
         $fuel_id=  $req->input("fuel_id");
         $fuel_name = $req->input("fuel_name");
         $date =date("Y-m-d H:i:s");
         $msg = array();

                  $sql = DB::table('tb_fuel')
                               ->insert(['fuel_id' => $fuel_id,
                                         'fuel_name' => $fuel_name,
                                         'update_by' =>$emp_id,
                                         'update_date'=>$date]);
                 if(!$sql){
                       $msg = array("type"=>"","success"=>false,"msg"=>"");

                 }else{
                     $msg = array("type"=>"","success"=>true,"msg"=>"");
                 }

                 return Response(json_encode($msg));
         }

         public function EditFuel(Request $req)
         {
           $emp_id =$req->input("id");
           $fuel_id =$req->input("fuel");
           return view('Models.Fuel.editfuel',['emp_id'=>$emp_id,'fuel_id'=>$fuel_id]);
         }

         public function UpdateFuelDB(Request $req)
         {
           date_default_timezone_set("Asia/Bangkok");
           $emp_id = strtoupper($req->input("emp_id")) ;
           $fuel_id=  $req->input("fuel_id");
           $fuel_name = $req->input("fuel_name");
           $date =date("Y-m-d H:i:s");
           $msg = array();

                    $sql = DB::table('tb_fuel')
                    ->where('fuel_id', '=' ,$fuel_id)
                    ->update(['fuel_name' => $fuel_name]);


                       $msg = array("type"=>"","success"=>true,"msg"=>"");





                   return Response(json_encode($msg));
           }

}
