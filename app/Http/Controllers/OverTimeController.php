<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class OverTimeController extends Controller
{
    //
    public function InsertBusOT(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
      $emp_id = $req->input('emp_id');
      $com_id = $req->input('com_id');
      $dep_id = $req->input('dep_id');
      $job_id = $req->input('job_id');
      $cdep = $req->input('job_id');
      $dep_id = $req->input('dep_id');
      $person = $req->input('count_person');
      $bk_date =date("Y-m-d H:i:s");
      $bk_start = str_replace("/","-",$req->input('bk_start_date'))." ".$req->input('bk_start_start');
      $bk_end = str_replace("/","-",$req->input('bk_start_date'))." ".$req->input('bk_end_start');
      $obj = $req->input('bk_obj');
      $note = $req->input('bk_note');
      $mtel = $req->input('mtel');
      $ttel = $req->input('ttel');
      $car = $req->input("car");
      if ($req->input('overtime') != "") {
          $overtime = $req->input('overtime');
      }else {
          $overtime = null;
      }
      $status = "success";
      // insert
          // gen_id
            $id = "";
            $ymd = date("ymd");
            $sql_booking = DB::table('tb_booking')->select('bk_id')->orderBy('bk_id','desc')->limit(1)->get();
          foreach ($sql_booking as $bk):
            $last_id = $bk->bk_id;
          endforeach;
            $old = explode("BK",$last_id);
            $old_ymd = substr($old[1],0,6);
            $old_id = substr($old[1],6,4);
            if($old_ymd == $ymd){
              $id = $old_id+1;
            }else{
              $id = 1;
            }
          $bk_id = "BK".$ymd.sprintf("%04d",$id);
          // end gen_id

          $sql_car = DB::table('tb_car')->where('car_id','=',$car)->get();
          foreach ($sql_car as $c) {
            $ctype = $c->ctype_id;
          }
          $driver= $req->input("driver");

          $merge = $req->input("overtimei");
          $bk_merge = "";
            for ($i=1; $i < substr_count($merge, 'BK'); $i++) {
              $merge = substr_replace($merge,';', 12*$i, 0);
            }
          if(strpos($merge,';')!==FALSE){
               $str = explode(";",$merge);
              }else{
                $str = array($merge);
              }
        for ($i=0; $i < count($str); $i++) {
          $sql_bk = DB::table("tb_booking")->where("bk_id",'=',$str[$i])->get();
                foreach ($sql_bk as $mbk) {
                  if ($mbk->bk_merge != '') {
                      $bk_merge = $mbk->bk_merge.";".$bk_id;
                  }else {
                    $bk_merge = $bk_id;
                  }
                }
                $sql_update = DB::table("tb_booking")
                              ->where("bk_id",'=',$str[$i])
                              ->update([
                                        "bk_status"=>"merge",
                                        "car_id"=>$car,
                                        "drive_id"=>$driver,
                                        "setcar_by"=>$emp_id,
                                        "setcar_date"=>$bk_date,
                                        "bk_merge"=>$bk_merge
                                      ]);

        }

                $sqlbooking =  DB::table('tb_booking')->insert(
                            [ 'bk_id' => "$bk_id",
                              'com_id' => "$com_id",
                              'bk_date' => "$bk_date",
                              'bk_start_start' => "$bk_start",
                              'bk_end_start' => "$bk_end",
                              'bk_percon' => "$person",
                              'bk_obj' => "$obj",
                              'dep_id' => "$dep_id",
                              'job_id' => "$job_id",
                              'dep_car'=> "$cdep",
                              'bk_mtel'=>"$mtel",
                              'emp_id' => "$emp_id",
                              'bk_status' => "$status",
                              'ctype_id' => "$ctype",
                              'approve_by' =>"SR600000000",
                              'approve_date'=>"$bk_date",
                              'setcar_by' =>"SR600000000",
                              'setcar_date'=>"$bk_date",
                              'car_id'=>"$car",
                              'drive_id'=>"$driver",
                              'bk_merge'=>"$merge",
                              'bk_ot'=>$overtime
                            ]);

          // add log
           $Sh = substr($req->input("bk_start_start"),0,2);
           $Eh = substr($req->input("bk_end_start"),0,2);

          //=======================================Car=======================================
            $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
            if (count($carlogid)>0) {
              foreach ($carlogid as $id) {
                  $logcar = $id->log_id;
              }
            }
            else {
              $logcar = 0;
            }

            $countcar = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car)->where('log_date', '=' ,substr($bk_start,0,10))->count();
            if ($countcar==1) {
              for ($j=$Sh; $j <= $Eh ; $j++) {
                $sqlCarUPDATE = DB::table('tb_log_car')
                                ->where('car_id', '=' ,$car)
                                ->where('dep_car','=',$cdep)
                                ->where('com_id','=',$com_id)
                                ->where('log_date', '=' ,substr($bk_start,0,10))
                                ->update([
                                             'log_'.(int)$j => $bk_id
                                        ]);
              }//for
            }//have log
            else {
               $sqllogcar = DB::table('tb_log_car')
                          ->insert([
                                    'log_id'=>$logcar+1,
                                    'dep_car'=>$cdep,
                                    'com_id'=>$com_id,
                                    'log_date'=>substr($bk_start,0,10),
                                    'car_id'=>$car
                                  ]);

              for ($j=$Sh; $j <= $Eh ; $j++) {
                $sqlCarUPDATE = DB::table('tb_log_car')
                              ->where('car_id', '=' ,$car)
                              ->where('com_id','=',$com_id)
                              ->where('dep_car','=',$cdep)
                              ->where('log_date', '=' ,substr($bk_start,0,10))
                              ->update([
                                        'log_'.(int)$j => $bk_id
                                ]);
              }//for
            }//no log
          //=======================================Endcar=======================================
          //=======================================Driver=======================================
            $countdriver = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$driver)->where('log_date', '=' ,substr($bk_start,0,10))->count();
            if ($countdriver==1) {
              for ($j=$Sh; $j <= $Eh ; $j++) {
                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                  ->where('drive_id', '=' ,$driver)
                                  ->where('dep_drive','=',$cdep)
                                  ->where('com_id','=',$com_id)
                                  ->where('log_date', '=' ,substr($bk_start,0,10))
                                  ->update([
                                            'log_'.(int)$j => $bk_id
                                          ]);
              }//for
            }//have log
            else {
              $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
              if (count($drivelogid)>0) {
                foreach ($drivelogid as $id) {
                    $logdriver = $id->log_id;
                }
              }
              else {
                $logdriver = 0;
              }
               $sqllogdriver = DB::table('tb_log_driver')
                              ->insert([
                                    'log_id'=>$logdriver+1,
                                    'dep_drive'=>$cdep,
                                    'com_id'=>$com_id,
                                    'log_date'=>substr($bk_start,0,10),
                                    'drive_id'=>$driver
                                  ]);
               for ($j=$Sh; $j <= $Eh ; $j++) {
                  $sqlDriveUPDATE = DB::table('tb_log_driver')
                                  ->where('drive_id', '=' ,$driver)
                                  ->where('com_id','=',$com_id)
                                  ->where('dep_drive','=',$cdep)
                                  ->where('log_date', '=' ,substr($bk_start,0,10))
                                  ->update([
                                            'log_'.(int)$j => $bk_id
                                          ]);
                  }//for
              }//no log
          //=======================================EndDriver=======================================

        if(!$sqlbooking and !$sqlCarUPDATE and !$sqlDriveUPDATE ){
          $msg = array("success"=>false,"msg"=>"");//2
        }else{
          $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id,'bk_merge'=>$merge);//1
        }
        return Response(json_encode($msg));
    }

}
