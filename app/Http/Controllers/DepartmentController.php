<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DepartmentController extends Controller
{
    //
    public function UPDATEDepartment(Request $req)
    {
      $emp_id = strtoupper($req->input("emp_id"));
      $com_id = $req->input("com_id") ;
      return view('Models.Department.updatedepartment',['emp_id'=>$emp_id,'com_id'=>$com_id]);

    }

    public function UPDATEDB(Request $req)
    {
        date_default_timezone_set("Asia/Bangkok");
        $emp_id = strtoupper($req->input("emp_id"));
        $com_id = $req->input("com_id") ;
        $ldap_username = $req->input("username")."@sr.com";
        $ldap_password = $req->input("password");
        $ldap_connection = ldap_connect('172.16.0.4');
        $msg = array();
        $date =date("Y-m-d H:i:s");
        if ($ldap_username == ''||$ldap_password =='') {
          $msg = array("type"=>"error","success"=>false,"msg"=>"กรุณากรอกชื่อผู้ใช้หรือรหัสผ่าน");
        }
        else {
        if (FALSE === $ldap_connection){
            // Uh-oh, something is wrong...
        }
        // We have to set this option for the version of Active Directory we are using.
        ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
        ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0); // We need this for doing an LDAP search.
        $bind = @ldap_bind($ldap_connection, $ldap_username, $ldap_password);

        if ($bind){
            $ldap_base_dn = 'DC=sr,DC=com';
            $search_filter = '(&(objectCategory=person)(samaccountname=*))';
            $attributes = array();
            $attributes[] = 'givenname';
            $attributes[] = 'mail';
            $attributes[] = 'samaccountname';
            $attributes[] = 'sn';
            $attributes[] = 'description';

            $result = ldap_search($ldap_connection, $ldap_base_dn, $search_filter, $attributes);
            if (FALSE !== $result){
                $entries = ldap_get_entries($ldap_connection, $result);
                for ($x=0; $x<$entries['count']; $x++){
                  $sqldep = DB::table('tb_department')->where('com_id','=',$com_id)->orderBy('dep_id','decs')->limit(1)->get();
                  foreach ($sqldep as $dep):
                   $id = $dep->dep_id;
                  endforeach;

                    if (!empty($entries[$x]['givenname'][0]) &&
                         // !empty($entries[$x]['mail'][0]) &&
                         !empty($entries[$x]['samaccountname'][0]) &&
                         !empty($entries[$x]['sn'][0])
                         // &&
                         // 'Shop' !== $entries[$x]['sn'][0] &&
                         // 'Account' !== $entries[$x]['sn'][0]
                        )
                    {
                      if (count($sqldep)>0) {
                        $Dep = substr($id, 0, 3);//DEP
                        $id1= substr($id, 4, 3)+1;//001
                      }//if count($sqldep)>0
                      else {
                        $Dep = 'DEP';
                        $id1 = '1';
                      }//eles count($sqldep)>0
                      $dep_id = $Dep.sprintf("%03d",$id1);

                      $dep_name = strtoupper(trim($entries[$x]['description'][0]));

                      $sql_chk = DB::table('tb_department')
                                  ->where('dep_name','=',strtoupper(trim($entries[$x]['description'][0])))
                                  ->get();

                      if (count($sql_chk) > 0) {
                        foreach ($sql_chk as $depchk) {
                          $up_dep = $depchk->dep_id;
                        }
                        $sqlUPDATE = DB::table('tb_department')
                                            ->where('dep_id', '=' ,$up_dep)
                                            ->where('com_id','=', $com_id)
                                            ->update(['dep_name' => $dep_name,
                                                      'update_by' =>$emp_id,
                                                      'update_date'=>$date]);

                      }//if $sql_chk > 0
                      else {
                        $sqlInsert = DB::table('tb_department')
                                      ->insert(['dep_id' => $dep_id,
                                                'com_id' => $com_id,
                                                'dep_name' => $dep_name,
                                                'update_by' =>$emp_id,
                                                'update_date'=>$date]);

                        }//else $sql_chk > 0
                      }//if  !empty($entries[$x]['givenname'][0]
                    }// for
                }//if FALSE !== $result
                ldap_unbind($ldap_connection); // Clean up after ourselves.
              $msg = array("type"=>"","success"=>true,"msg"=>"อัพเดทข้อมูลหน่วยงานสำเร็จแล้ว");
            }//if $bind
            else {
              $msg = array("type"=>"error","success"=>false,"msg"=>"ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง");
            }//else $bind

          }//has value username password

            return Response(json_encode($msg));
      }

      public function EditDep(Request $req)
      {
        $emp_id =$req->input("emp_id");
        $dep_id =$req->input("dep");
        return view('Models.Department.editdepartment',['emp_id'=>$emp_id,'dep_id'=>$dep_id]);

      }

      public function UpdateDepDB(Request $req)
      {
        date_default_timezone_set("Asia/Bangkok");
        $emp_id = strtoupper($req->input("emp_id")) ;
        $dep_id=  $req->input("dep_id");
        $dep_name = $req->input("dep_name");
        $msg = array();
        $date =date("Y-m-d H:i:s");

                 $sqlUPDATE = DB::table('tb_department')
                               ->where('dep_id', '=' ,$dep_id)
                               ->update(['dep_name' => $dep_name,
                                         'update_by' =>$emp_id,
                                         'update_date'=>$date]);

                  if (!$sqlUPDATE) {
                    $msg = array("type"=>"","success"=>false,"msg"=>"");
                  }
                  else {
                    $msg = array("type"=>"","success"=>true,"msg"=>"");
                  }

                return Response(json_encode($msg));
        }

}
