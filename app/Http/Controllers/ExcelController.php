<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use View;
use DateTime;
use PHPExcel_Style_Border;

class ExcelController extends Controller
{
  public function statusText($status)
  {
    if($status == "wait"){
        $status = "รอการอนุมัติ";
      }else if($status== "approve"){
        $status = "รอการจัดรถ";
      }else if($status == "success"){
        $status = "จัดรถสำเร็จ";
      }elseif ($status =="complete"){
        $status = "ดำเนินการเสร็จสิ้น";
      }else if($status == "merge"){
        $status = "สำเร็จ(ร่วมเดินทาง)";
      }else if($status == "eject"){
        $status = "ยกเลิกการจอง";
      }else if($status == "ejectcar"){
        $status = "ยกเลิกการเดินทาง";
      }else if($status == "nonecar"){
        $status = "ไม่มีรถ";
      }
      return $status ;
  }

  public function reportExcelSuccess()
  {
    date_default_timezone_set("Asia/Bangkok");
    $date_now = date("Y/m/d");
    $sqlbooking_sql = DB::table('tb_booking')
                  ->join("tb_car_type",function($join){
                        $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                        $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                    })
                  ->join("tb_car",function($join){
                        $join->on("tb_booking.car_id","=","tb_car.car_id");
                        $join->on("tb_booking.com_id","=","tb_car.com_id");
                    })
                  ->join("tb_driver",function($join){
                        $join->on("tb_booking.drive_id","=","tb_driver.drive_id");
                        $join->on("tb_booking.com_id","=","tb_driver.com_id");
                    })
                  ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                  // ->where('tb_booking.bk_status', '=', 'success')
                  ->where('tb_booking.bk_end_start','<',$date_now)
                  ->orderBy('bk_end_start')->get();
                  // print_r($sqlbooking_sql);exit;
    return Excel::create('content', function($excel) use ($sqlbooking_sql) {
            $excel->sheet('sheet1', function($sheet) use ($sqlbooking_sql) {
                      $sheet->setCellValue('A1', "เลขอ้างอิง");
                      $sheet->setCellValue('B1', "ผู้บันทึก");
                      $sheet->setCellValue('C1', "สถานะ");
                      $sheet->setCellValue('D1',"ชื่อผู้ขอ");
                      $sheet->setCellValue('E1',"เบอร์โทรศัพท์ผู้ขอ");
                      $sheet->setCellValue('F1', "ชื่อผู้ใช้งาน");
                      $sheet->setCellValue('G1', "เบอร์โทรศัพท์ผู้ใช้งาน");
                      $sheet->setCellValue('H1', "Package");
                      $sheet->setCellValue('I1', "เริ่มใช้งาน");
                      $sheet->setCellValue('J1', "ใช้งานถึง");
                      $sheet->setCellValue('K1', "ปิดงาน");
                      $sheet->setCellValue('L1', "ระยะเวลา");
                      $sheet->setCellValue('M1', "บันทึกปิดงาน");
                  for ($j=0,$i=2; $j < count($sqlbooking_sql); $j++,$i++){
                      $sheet->setCellValue('A'.$i, $sqlbooking_sql[$j]->bk_id);
                      $sheet->setCellValue('B'.$i, $sqlbooking_sql[$j]->emp_fname." ".$sqlbooking_sql[$j]->emp_lname);
                      $sheet->setCellValue('C'.$i, $sqlbooking_sql[$j]->bk_status);
                      $sql_requester = DB::table('tb_requester')->where('req_id','=',$sqlbooking_sql[$j]->req_id)->get();
                      foreach ($sql_requester as $req) {
                        $sheet->setCellValue('D'.$i, $req->fname." ".$req->lname);
                        $sheet->setCellValue('E'.$i, $req->tel);
                      }
                      $sql_customer = DB::table('tb_customer')->where('cus_id','=',$sqlbooking_sql[$j]->cus_id)->get();
                      foreach ($sql_customer as $cus) {
                        $sheet->setCellValue('F'.$i, $cus->fname." ".$cus->lname);
                        $sheet->setCellValue('G'.$i, $cus->tel);
                      }
                      $sheet->setCellValue('H'.$i, "package ".$sqlbooking_sql[$j]->bk_package);
                      $sheet->setCellValue('I'.$i, $sqlbooking_sql[$j]->bk_start_start);
                      $sheet->setCellValue('J'.$i, $sqlbooking_sql[$j]->bk_end_start);
                      if ($sqlbooking_sql[$j]->bk_close == 'null') {
                        $sheet->setCellValue('K'.$i, $sqlbooking_sql[$j]->bk_close);
                        $diff=date_diff($sqlbooking_sql[$j]->bk_close,$sqlbooking_sql[$j]->bk_end_start);
                        $sheet->setCellValue('L'.$i, $diff);
                        $sheet->setCellValue('M'.$i, $sqlbooking_sql[$j]->bk_close_note);
                      }
                   }
            });

        })->download('xlsx');
  }

  public function reportExcelComplete()
  {
    date_default_timezone_set("Asia/Bangkok");
    $date_now = date("Y/m/d");
    $sqlbooking_sql = DB::table('tb_booking')
                  ->join("tb_car_type",function($join){
                        $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                        $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                    })
                  ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                  ->where('tb_booking.bk_status', '=', 'complete')
                  ->where('tb_booking.bk_end_start','<',$date_now)
                  ->orderBy('bk_end_start')->get();

                  return Excel::create('content', function($excel) use ($sqlbooking_sql) {
                          $excel->sheet('sheet1', function($sheet) use ($sqlbooking_sql) {
                                    // $sheet->mergeCells('A1:E1');
                                    $sheet->setCellValue('A1', "เลขอ้างอิง");
                                    $sheet->setCellValue('B1', "ผู้บันทึก");
                                    $sheet->setCellValue('C1', "สถานะ");
                                    $sheet->setCellValue('D1',"ชื่อผู้ขอ");
                                    $sheet->setCellValue('E1',"เบอร์โทรศัพท์ผู้ขอ");
                                    $sheet->setCellValue('F1', "ชื่อผู้ใช้งาน");
                                    $sheet->setCellValue('G1', "เบอร์โทรศัพท์ผู้ใช้งาน");
                                    $sheet->setCellValue('H1', "Package");
                                    $sheet->setCellValue('I1', "เริ่มใช้งาน");
                                    $sheet->setCellValue('J1', "ใช้งานถึง");
                                    $sheet->setCellValue('K1', "ปิดงาน");
                                    $sheet->setCellValue('L1', "ระยะเวลา");
                                    $sheet->setCellValue('M1', "บันทึกปิดงาน");
                                for ($j=0,$i=2; $j < count($sqlbooking_sql); $j++,$i++){
                                    $sheet->setCellValue('A'.$i, $sqlbooking_sql[$j]->bk_id);
                                    $sheet->setCellValue('B'.$i, $sqlbooking_sql[$j]->emp_fname." ".$sqlbooking_sql[$j]->emp_lname);
                                    $sheet->setCellValue('C'.$i, $sqlbooking_sql[$j]->bk_status);
                                    $sql_requester = DB::table('tb_requester')->where('req_id','=',$sqlbooking_sql[$j]->req_id)->get();
                                    foreach ($sql_requester as $req) {
                                      $sheet->setCellValue('D'.$i, $req->fname." ".$req->lname);
                                      $sheet->setCellValue('E'.$i, $req->tel);
                                    }
                                    $sql_customer = DB::table('tb_customer')->where('cus_id','=',$sqlbooking_sql[$j]->cus_id)->get();
                                    foreach ($sql_customer as $cus) {
                                      $sheet->setCellValue('F'.$i, $cus->fname." ".$cus->lname);
                                      $sheet->setCellValue('G'.$i, $cus->tel);
                                    }
                                    $sheet->setCellValue('H'.$i, "package ".$sqlbooking_sql[$j]->bk_package);
                                    $sheet->setCellValue('I'.$i, $sqlbooking_sql[$j]->bk_start_start);
                                    $sheet->setCellValue('J'.$i, $sqlbooking_sql[$j]->bk_end_start);
                                    if ($sqlbooking_sql[$j]->bk_close == 'null') {
                                      $sheet->setCellValue('K'.$i, $sqlbooking_sql[$j]->bk_close);
                                      $diff=date_diff($sqlbooking_sql[$j]->bk_close,$sqlbooking_sql[$j]->bk_end_start);
                                      $sheet->setCellValue('L'.$i, $diff);
                                      $sheet->setCellValue('M'.$i, $sqlbooking_sql[$j]->bk_close_note);
                                    }
                                 }
                          });

                      })->download('xlsx');
  }

  public function reportExcel(Request $req)
  {
    $type = $req->input('type');
    $typeSearch = $req->input('typeSearch');
    // $txtSearch = $req->input('txtSearch');
    $txtSearch = "";
    $bk_status = $req->input('bk_status');
    $date1 = $req->input('date1');
    $date2 = $req->input('date2');
    if ($req->input('month')<10) {
      $month = '0'.$req->input('month');
    }
    else {
      $month = $req->input('month');
    }
    $date3 = $req->input('year1').'-'.$month;
    $date4 = $req->input('year2');
    $com_id = $req->input('com_id');
    $sql = DB::table('tb_booking')
                  ->join("tb_car_type",function($join){
                        $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                        $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                    })
                  ->join("tb_car",function($join){
                        $join->on("tb_booking.car_id","=","tb_car.car_id");
                        $join->on("tb_booking.com_id","=","tb_car.com_id");
                    })
                  ->join("tb_driver",function($join){
                        $join->on("tb_booking.drive_id","=","tb_driver.drive_id");
                        $join->on("tb_booking.com_id","=","tb_driver.com_id");
                    })
                  ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                  ->where('tb_booking.com_id','=',$com_id);

    if ($type == 0) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql = $sql;
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql;
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status);
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql;
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status);
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch);
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status);
        }
      }//$typeSearch == 2
    }
    // รายวัน
    elseif ($type == 1) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date1.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date1.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date1.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date1.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
      }//$typeSearch == 2
    }
    // รายเดือน
    elseif ($type == 2) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date3.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date3.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date3.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date3.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
      }//$typeSearch == 2

    }
    // รายปี
    elseif ($type == 3) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date4.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date4.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date4.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date4.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
      }//$typeSearch == 2
    }
    //ช่วงเวลา
    else {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->whereBetween('bk_start_start', [$date1, $date2]);
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->whereBetween('bk_start_start', [$date1, $date2]);
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->whereBetween('bk_start_start', [$date1, $date2]);
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->whereBetween('bk_start_start', [$date1, $date2]);
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
      }//$typeSearch == 2

    }

    $qrybk = $sql->get();
    $numbk = $sql->count();

    if ($numbk > 0) {
      $msg = $msg = array("type"=>"","success"=>true,"msg"=>"สำเร็จ","data"=>$qrybk);
      // $msg = $msg = array("type"=>"","success"=>true,"msg"=>"รายการทั้งหมด".$numbk."รายการ","data"=>$qrybk);
    }
    else{
      $msg = $msg = array("type"=>"","success"=>false,"msg"=>"ไม่พบกิจกรรม","data"=>"");
    }
    return Response(json_encode($msg));
  }

  public function ReportDownload(Request $req)
  {
    date_default_timezone_set("Asia/Bangkok");
    $date_now = date("YmdHi");
    $type = $req->input('type');
    $typeSearch = $req->input('typeSearch');
    // $txtSearch = $req->input('txtSearch');
    $txtSearch = "";
    $bk_status = $req->input('bk_status');
    $date1 = $req->input('date1');
    $date2 = $req->input('date2');
    if ($req->input('month')<10) {
      $month = '0'.$req->input('month');
    }
    else {
      $month = $req->input('month');
    }
    $date3 = $req->input('year1').'-'.$month;
    $date4 = $req->input('year2');
    $com_id = $req->input('com_id');
    $sql = DB::table('tb_booking')
                  ->join("tb_car_type",function($join){
                        $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                        $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                    })
                  ->leftJoin("tb_car",function($join){
                        $join->on("tb_booking.car_id","=","tb_car.car_id");
                        $join->on("tb_booking.com_id","=","tb_car.com_id");
                    })
                  ->leftJoin("tb_driver",function($join){
                        $join->on("tb_booking.drive_id","=","tb_driver.drive_id");
                        $join->on("tb_booking.com_id","=","tb_driver.com_id");
                    })
                  ->leftJoin("tb_package",function($join){
                        $join->on("tb_booking.bk_package","=","tb_package.package_type");
                        $join->on("tb_booking.com_id","=","tb_package.com_id");
                        $join->on("tb_booking.ctype_id","=","tb_package.ctype_id");
                    })
                  ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                  ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id')
                  ->where('tb_booking.com_id','=',$com_id);

    if ($type == 0) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql = $sql;
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql;
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status);
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql;
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status);
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch);
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status);
        }
      }//$typeSearch == 2
    }
    // รายวัน
    elseif ($type == 1) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date1.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date1.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date1.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date1.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
      }//$typeSearch == 2
    }
    // รายเดือน
    elseif ($type == 2) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date3.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date3.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date3.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date3.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
      }//$typeSearch == 2

    }
    // รายปี
    elseif ($type == 3) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date4.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date4.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date4.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date4.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
      }//$typeSearch == 2
    }
    //ช่วงเวลา
    else {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->whereBetween('bk_start_start', [$date1, $date2]);
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->whereBetween('bk_start_start', [$date1, $date2]);
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->whereBetween('bk_start_start', [$date1, $date2]);
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->whereBetween('bk_start_start', [$date1, $date2]);
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
      }//$typeSearch == 2

    }
    $qrybk = $sql->orderBy('bk_id','desc')->get();
    // $qrybk = $sql->orderBy('bk_id','desc')->limit(1)->get();
    // print_r($qrybk);exit;
    return Excel::create('ReportCarpools'.$date_now, function($excel) use ($qrybk) {
            $excel->sheet('sheet1', function($sheet) use ($qrybk) {

                  $sheet->getStyle('A:ZZ')->getAlignment()->applyFromArray(
                        array('horizontal' => 'center','vertical' => 'center') //left,right,center & vertical
                      );
                  $sheet->setStyle(array(
                          'font' => array(
                              'name'      =>  'Angsana New',
                              'size'      =>  16
                          )
                      ));
                  // $sheet->setAutoFit(true);
                  // $sheet->getStyle('A:ZZ')->getAlignment()->setWrapText(true);
                  $sheet->setMergeColumn(array(
                              'columns' => array('A','B','C','D','E','F','G','H','Q','R','S','X','AC','AD','AE','AF','AG','AH','AI'),
                              'rows' => array(array(1,2),array(1,2),array(1,2),array(1,2),array(1,2),array(1,2),
                                              array(1,2),array(1,2),array(1,2),array(1,2),array(1,2),array(1,2),
                                              array(1,2),array(1,2),array(1,2),array(1,2),array(1,2),array(1,2),array(1,2))
                          ));

                  $sheet->setColumnFormat(array(
                              'R' => '0.00',
                              'U' => '0.00',
                              'W' => '0.00',
                              'X' => '0.00',
                              'Y' => '0.00',
                              'Z' => '0.00',
                              'AA' => '0.00',
                              'AB' => '0.00',
                              'AC' => '0.00',
                              'AD' => '0.00',
                              'AE' => '0.00',
                              'AF' => '0.00',
                              'AG' => '0.00'
                  ));
                  $sheet->setCellValue('A1', "Booking No.");
                  $sheet->setCellValue('B1', "Booking Date");
                  $sheet->setCellValue('C1', "Job No.");
                  $sheet->setCellValue('D1', "Job Date");

                  $sheet->setCellValue('E1', "ชื่อลูกค้า");
                  $sheet->setCellValue('F1', "อ้างถึงเลขที่ใบงานลูกค้า");
                  $sheet->setCellValue('G1', "ทะเบียนรถ");
                  $sheet->setCellValue('H1', "คนขับ");

                  $sheet->mergeCells('I1:L1');
                  $sheet->setCellValue('I1', "วันที่ขอ");
                  $sheet->setCellValue('I2', "วันที่เริ่ม");
                  $sheet->setCellValue('J2', "วันที่สิ้นสุด");
                  $sheet->setCellValue('K2', "เวลาเริ่ม");
                  $sheet->setCellValue('L2', "เวลาสิ้นสุด");

                  $sheet->mergeCells('M1:P1');
                  $sheet->setCellValue('M1', "วันที่ใช้จริง");
                  $sheet->setCellValue('M2', "วันที่เริ่ม");
                  $sheet->setCellValue('N2', "วันที่สิ้นสุด");
                  $sheet->setCellValue('O2', "เวลาเริ่ม");
                  $sheet->setCellValue('P2', "เวลาสิ้นสุด");

                  $sheet->setCellValue('Q1', "Package");
                  $sheet->setCellValue('R1', "ราคาตาม Package");
                  $sheet->setCellValue('S1', "จำนวน ชม. ที่ใช้");

                  $sheet->mergeCells('T1:U1');
                  $sheet->setCellValue('T1', "ส่วนต่าง Package");
                  $sheet->setCellValue('T2', "จำนวน ชม.");
                  $sheet->setCellValue('U2', "จำนวนเงิน");

                  $sheet->mergeCells('V1:W1');
                  $sheet->setCellValue('V1', "ค่าล่วงเวลา");
                  $sheet->setCellValue('V2', "จำนวน ชม.");
                  $sheet->setCellValue('W2', "จำนวนเงิน");

                  $sheet->setCellValue('X1', "รวมค่าบริการ");

                  $sheet->mergeCells('Y1:AB1');
                  $sheet->setCellValue('Y1', "ค่าบริการอื่น");
                  $sheet->setCellValue('Y2', "ค่าทางด่วน");
                  $sheet->setCellValue('Z2', "ค่าทีจอดรถ");
                  $sheet->setCellValue('AA2', "ค่าน้ำมัน");
                  $sheet->setCellValue('AB2', "ค่าที่พัก");

                  $sheet->setCellValue('AC1', "รวม ค่าบริการอื่น");
                  $sheet->setCellValue('AD1', "ค่าดำเนินการ 5%");
                  $sheet->setCellValue('AE1', "ค่าบริการอื่นๆ + ค่าดำเนินการ");
                  $sheet->setCellValue('AF1', "อื่นๆ");
                  $sheet->setCellValue('AG1', "Total");
                  $sheet->setCellValue('AH1', "Status");
                  $sheet->setCellValue('AI1', "หมายเหตุ");

              for ($j=0,$i=3; $j < count($qrybk); $j++,$i++){
                  $sheet->setCellValue('A'.$i, $qrybk[$j]->bk_id);
                  $sheet->setCellValue('B'.$i, date_format(date_create($qrybk[$j]->bk_date),"d/m/Y"));
                  $sheet->setCellValue('C'.$i, $qrybk[$j]->bk_job);
                  $sheet->setCellValue('D'.$i, date_format(date_create($qrybk[$j]->bk_close_at),"d/m/Y"));
                  $sheet->setCellValue('E'.$i, $qrybk[$j]->com_name);
                  $sheet->setCellValue('F'.$i, $qrybk[$j]->bk_application_no);
                  $sheet->setCellValue('G'.$i, $qrybk[$j]->car_number);
                  $sheet->setCellValue('H'.$i, $qrybk[$j]->drive_fname.' '.$qrybk[$j]->drive_lname);
                  $sheet->setCellValue('I'.$i, date_format(date_create($qrybk[$j]->bk_start_start),"d/m/Y"));
                  $sheet->setCellValue('J'.$i, date_format(date_create($qrybk[$j]->bk_end_start),"d/m/Y"));
                  $sheet->setCellValue('K'.$i, date_format(date_create($qrybk[$j]->bk_start_start),"H:i"));
                  $sheet->setCellValue('L'.$i, date_format(date_create($qrybk[$j]->bk_end_start),"H:i"));

                  if ($qrybk[$j]->bk_close_at != '' || $qrybk[$j]->bk_close_at != null) {
                    $sheet->setCellValue('M'.$i, date_format(date_create($qrybk[$j]->bk_start_close),"d/m/Y"));
                    $sheet->setCellValue('N'.$i, date_format(date_create($qrybk[$j]->bk_end_close),"d/m/Y"));
                    $sheet->setCellValue('O'.$i, date_format(date_create($qrybk[$j]->bk_start_close),"H:i"));
                    $sheet->setCellValue('P'.$i, date_format(date_create($qrybk[$j]->bk_end_close),"H:i"));
                  }

                  $sheet->setCellValue('Q'.$i, $qrybk[$j]->bk_package);

                  $datetime1 = new DateTime($qrybk[$j]->bk_start_start);
                  $datetime2 = new DateTime($qrybk[$j]->bk_end_start);
                  $diff_time = $datetime2->diff($datetime1);
                  $hours_cal = $diff_time->h;
                  $minutes= $diff_time->i;
                  $hours_cal = $hours_cal + ($diff_time->days*24);
                  $hours_cal = ($minutes > 0) ? $hours_cal+1 : $hours_cal;
                  $hours_close = 0;
                  if ($qrybk[$j]->bk_end_close != '') {
                    $datetime1 = new DateTime($qrybk[$j]->bk_end_close);
                    if ($qrybk[$j]->bk_start_close !='') {
                      $datetime2 = new DateTime($qrybk[$j]->bk_start_close);
                    }else {
                      $datetime2 = new DateTime($qrybk[$j]->bk_start_start);
                    }
                    $diff = $datetime2->diff($datetime1);
                    $hours_close = $diff->h;
                    $minutes= $diff->i;
                    $hours_close = $hours_close + ($diff->days*24);
                    $hours_close = ($minutes > 0) ? $hours_close+1 : $hours_close;
                  }
                  $max = max($hours_cal,$hours_close);

                  if ($qrybk[$j]->bk_package == "D") {
                    if ( $max < (int)$qrybk[$j]->package_mininum) {
                      $total = (int)($qrybk[$j]->package_mininum/24)*$qrybk[$j]->price;
                    }else {
                      $total = (ceil($max/24)*$qrybk[$j]->price);
                    }
                  }else {
                    if ($max < (int)$qrybk[$j]->package_mininum) {
                      $total = $qrybk[$j]->package_mininum*$qrybk[$j]->price;
                    }else {
                      $total = $max*$qrybk[$j]->price;
                    }
                  }

                  $sheet->setCellValue('R'.$i, $qrybk[$j]->price_minimum);
                  $sheet->setCellValue('S'.$i, $max);
                  if (($total-$qrybk[$j]->price_minimum) >= 0) {
                    $total_diff = ($total-$qrybk[$j]->price_minimum );
                  }else {
                    $total_diff = 0;
                  }
                  if (($max-$qrybk[$j]->package_mininum)>=0) {
                    $hours_diff =  $max-$qrybk[$j]->package_mininum;
                  }else {
                    $hours_diff = 0;
                  }
                  if ($qrybk[$j]->bk_status != 'ejectcar'&& $qrybk[$j]->bk_status != 'eject') {
                      $sheet->setCellValue('T'.$i, $hours_diff);
                      $sheet->setCellValue('U'.$i, $total_diff);
                      if($qrybk[$j]->ot){
                      $sheet->setCellValue('V'.$i, $qrybk[$j]->support_ot);
                      $sheet->setCellValue('W'.$i, ($qrybk[$j]->support_ot*170));
                      }
                      $sheet->setCellValue('X'.$i, "=R".$i."+U".$i."+W".$i);
                      $sheet->setCellValue('Y'.$i, $qrybk[$j]->bk_expressway);
                      $sheet->setCellValue('Z'.$i, $qrybk[$j]->bk_parking);
                      $sheet->setCellValue('AA'.$i, $qrybk[$j]->bk_gasoline);
                      $sheet->setCellValue('AB'.$i, $qrybk[$j]->support_hotel);
                      if ($qrybk[$j]->bk_package == "A") {
                        $sheet->setCellValue('AC'.$i, "=Y".$i."+Z".$i."+AB".$i);
                      }else {
                      $sheet->setCellValue('AC'.$i, "=Y".$i."+Z".$i."+AA".$i."+AB".$i);
                      }
                      $sheet->setCellValue('AD'.$i, "=AC".$i."*(".$qrybk[$j]->extra_percent."/100)");
                      $sheet->setCellValue('AE'.$i, "=AC".$i."+AD".$i);
                  }else {
                    $sheet->mergeCells('T'.$i.':AE'.$i);
                    $sheet->setCellValue('T'.$i, 'ยกเลิก');
                  }
                  $sheet->setCellValue('AF'.$i, $qrybk[$j]->bk_service_charge);
                  if ($qrybk[$j]->bk_status != 'ejectcar' && $qrybk[$j]->bk_status != 'eject') {
                    $sheet->setCellValue('AG'.$i, "=X".$i."+AE".$i );
                  }else {
                    $sheet->setCellValue('AG'.$i, "=AF".$i );
                  }



                  $status = '';
                  if($qrybk[$j]->bk_status == "wait"){
                      // $status = "รอการอนุมัติ";
                      $status = "รอการจัดรถ";
                    }else if($qrybk[$j]->bk_status == "approve"){
                      $status = "รอการจัดรถ";
                    }else if($qrybk[$j]->bk_status == "success"){
                      $status = "จัดรถสำเร็จ";
                    }elseif ($qrybk[$j]->bk_status =="complete"){
                      $status = "ดำเนินการเสร็จสิ้น";
                    }else if($qrybk[$j]->bk_status == "eject"){
                      $status = "ยกเลิกการจอง";
                    }else if($qrybk[$j]->bk_status == "ejectcar"){
                      $status = "ยกเลิกการเดินทาง";
                    }else if($qrybk[$j]->bk_status == "nonecar"){
                      $status = "ไม่มีรถ";
                    }
                  $sheet->setCellValue('AH'.$i, $status);
                  if ($qrybk[$j]->bk_status == 'ejectcar') {
                    $sheet->setCellValue('AI'.$i, $qrybk[$j]->bk_service_note);
                  }elseif ( $qrybk[$j]->bk_status == 'eject') {
                    $sheet->setCellValue('AI'.$i, $qrybk[$j]->bk_reasons);
                  }else {
                    $sheet->setCellValue('AI'.$i, $qrybk[$j]->bk_close_note);
                  }

              }
              $sheet->setCellValue('R'.$i,"=SUM(R3:R".($i-1).")");
              $sheet->setCellValue('X'.$i,"=SUM(X3:X".($i-1).")");
              $sheet->setCellValue('AC'.$i,"=SUM(AC3:AC".($i-1).")");
              $sheet->setCellValue('AD'.$i,"=SUM(AD3:AD".($i-1).")");
              $sheet->setCellValue('AE'.$i,"=SUM(AE3:AE".($i-1).")");
              $sheet->setCellValue('AF'.$i,"=SUM(AF3:AF".($i-1).")");
              $sheet->setCellValue('AG'.$i,"=SUM(AG3:AG".($i-1).")");

            });

        })->download('xlsx');

  }



  public function ReportFirst(Request $req)
  {
    $type = $req->input('type');
    $typeSearch = $req->input('typeSearch');
    // $txtSearch = $req->input('txtSearch');
    $txtSearch = "";
    $bk_status = $req->input('bk_status');
    $date1 = $req->input('date1');
    $date2 = $req->input('date2');
    if ($req->input('month')<10) {
      $month = '0'.$req->input('month');
    }
    else {
      $month = $req->input('month');
    }
    $date3 = $req->input('year1').'-'.$month;
    $date4 = $req->input('year2');
    $com_id = $req->input('com_id');
    $sql = DB::table('tb_booking')
                  ->join("tb_car_type",function($join){
                        $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                        $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                    })
                  ->leftJoin("tb_car",function($join){
                        $join->on("tb_booking.car_id","=","tb_car.car_id");
                        $join->on("tb_booking.com_id","=","tb_car.com_id");
                    })
                  ->leftJoin("tb_driver",function($join){
                        $join->on("tb_booking.drive_id","=","tb_driver.drive_id");
                        $join->on("tb_booking.com_id","=","tb_driver.com_id");
                    })
                  ->leftJoin("tb_package",function($join){
                        $join->on("tb_booking.bk_package","=","tb_package.package_type");
                        $join->on("tb_booking.com_id","=","tb_package.com_id");
                        $join->on("tb_booking.ctype_id","=","tb_package.ctype_id");
                    })
                  ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                  ->where('tb_booking.com_id','=',$com_id);

    if ($type == 0) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql = $sql;
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql;
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status);
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql;
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status);
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch);
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status);
        }
      }//$typeSearch == 2
    }
    // รายวัน
    elseif ($type == 1) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date1.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date1.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date1.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date1.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
        }
      }//$typeSearch == 2
    }
    // รายเดือน
    elseif ($type == 2) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date3.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date3.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date3.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date3.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
        }
      }//$typeSearch == 2

    }
    // รายปี
    elseif ($type == 3) {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date4.'%');
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->where('bk_start_start','like',$date4.'%');
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->where('bk_start_start','like',$date4.'%');
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date4.'%');
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
        }
      }//$typeSearch == 2
    }
    //ช่วงเวลา
    else {
      if ($typeSearch == 0) {
        // ทั้งหมด
        $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->whereBetween('bk_start_start', [$date1, $date2]);
        // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
      }//$typeSearch == 0
      elseif ($typeSearch == 1) {
        // สถานะการจอง
        if ($bk_status == 'All') {
          // ทั้งหมด
          $sql = $sql->whereBetween('bk_start_start', [$date1, $date2]);
        }
        else {
          $sql = $sql->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
      }//$typeSearch == 1
      elseif ($typeSearch == 2) {
        // ชื่อคนจอง
        if ($txtSearch == 'All' && $bk_status == 'All') {
            // ชื่อคนจองว่าง สถานะทั้งหมด
            $sql = $sql->whereBetween('bk_start_start', [$date1, $date2]);
        }
        elseif ($txtSearch == 'All' && $bk_status <> 'All') {
          // ชื่อคนจองว่าง มีสถานะ
          $sql = $sql->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
        elseif ($txtSearch <> 'All' && $bk_status == 'All') {
          // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
          $sql = $sql->where('emp_id','=',$txtSearch)->whereBetween('bk_start_start', [$date1, $date2]);
        }
        else {
            // ชื่อคนจองไม่ว่าง มีสถานะ
          $sql = $sql->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
        }
      }//$typeSearch == 2

    }

    $qrybk = $sql->orderBy('bk_id','desc')->get();
    return Excel::create('content', function($excel) use ($qrybk) {
            $excel->sheet('sheet1', function($sheet) use ($qrybk) {
                  $sheet->setFontSize(16);

                  $sheet->getStyle('P')->getAlignment()->setWrapText(true);
                  $sheet->getStyle('A:ZZ')->getAlignment()->applyFromArray(
                    array('horizontal' => 'center','vertical' => 'center') //left,right,center & vertical
                  );
                  $sheet->setCellValue('A1', "เลขอ้างอิง");
                  $sheet->setCellValue('B1', "ผู้บันทึก");
                  $sheet->setCellValue('C1', "สถานะ");
                  $sheet->setCellValue('D1',"ชื่อผู้ขอ");
                  $sheet->setCellValue('E1',"เบอร์โทรศัพท์ผู้ขอ");
                  $sheet->setCellValue('F1', "ชื่อผู้ใช้งาน");
                  $sheet->setCellValue('G1', "เบอร์โทรศัพท์ผู้ใช้งาน");
                  $sheet->setCellValue('H1', "Package");
                  $sheet->setCellValue('I1', "ประเภทรถ");
                  $sheet->setCellValue('J1', "ทะเบียนรถ");
                  $sheet->setCellValue('K1', "คนขับ");
                  $sheet->setCellValue('L1', "เบอร์โทรคนขับ");
                  $sheet->setCellValue('M1', "เริ่มใช้งาน");
                  $sheet->setCellValue('N1', "ใช้งานถึง");
                  $sheet->setCellValue('O1', "ระยะเวลาในระบบ");
                  $sheet->setCellValue('P1', "ปิดงาน");
                  $sheet->setCellValue('Q1', "ระยะเวลาใช้จริง");
                  $sheet->setCellValue('R1', "บันทึกปิดงาน");
                  $sheet->setCellValue('S1', "สถานที่รับส่ง");
                  $sheet->setCellValue('T1', "สถานที่เดินทาง");
                  $sheet->setCellValue('U1', "ราคา(บาท/ชั่วโมง)");
                  $sheet->setCellValue('V1', "ราคาสุทธิ");

              for ($j=0,$i=2; $j < count($qrybk); $j++,$i++){
                  $sheet->setCellValue('A'.$i, $qrybk[$j]->bk_id);
                  $sheet->setCellValue('B'.$i, $qrybk[$j]->emp_fname." ".$qrybk[$j]->emp_lname);
                  $sheet->setCellValue('C'.$i, $qrybk[$j]->bk_status);
                  $sql_requester = DB::table('tb_requester')->where('req_id','=',$qrybk[$j]->req_id)->get();
                  foreach ($sql_requester as $req) {
                    $sheet->setCellValue('D'.$i, $req->fname." ".$req->lname);
                    $sheet->setCellValue('E'.$i, $req->tel);
                  }
                  $sql_customer = DB::table('tb_customer')->where('cus_id','=',$qrybk[$j]->cus_id)->get();
                  foreach ($sql_customer as $cus) {
                    $sheet->setCellValue('F'.$i, $cus->fname." ".$cus->lname);
                    $sheet->setCellValue('G'.$i, $cus->tel);
                  }
                  $sheet->setCellValue('H'.$i, $qrybk[$j]->bk_package);
                  $sheet->setCellValue('I'.$i,$qrybk[$j]->ctype_name);
                  $sheet->setCellValue('J'.$i,$qrybk[$j]->car_number);
                  $sheet->setCellValue('K'.$i,$qrybk[$j]->drive_fname." ".$qrybk[$j]->drive_lname);
                  $sheet->setCellValue('L'.$i,$qrybk[$j]->drive_tel);
                  $sheet->setCellValue('M'.$i, $qrybk[$j]->bk_start_start);
                  $sheet->setCellValue('N'.$i, $qrybk[$j]->bk_end_start);
                  $datetime1 = new DateTime($qrybk[$j]->bk_start_start);
                  $datetime2 = new DateTime($qrybk[$j]->bk_end_start);
                  $diff_time = $datetime2->diff($datetime1);
                  $hours_cal = $diff_time->h;
                  $minutes= $diff_time->i;
                  $hours_cal = $hours_cal + ($diff_time->days*24);
                  $hours_cal = ($minutes > 0) ? $hours_cal+1 : $hours_cal;
                  $sheet->setCellValue('O'.$i, $hours_cal);
                  $hours_close = 0;
                  if ($qrybk[$j]->bk_close != '') {
                    $sheet->setCellValue('P'.$i, $qrybk[$j]->bk_close);
                    $datetime1 = new DateTime($qrybk[$j]->bk_start_start);
                    $datetime2 = new DateTime($qrybk[$j]->bk_close);
                    $diff = $datetime2->diff($datetime1);
                    $hours_close = $diff->h;
                    $minutes= $diff->i;
                    $hours_close = $hours_close + ($diff->days*24);
                    $hours_close = ($minutes > 0) ? $hours_close+1 : $hours_close;
                    $sheet->setCellValue('Q'.$i, $hours_close);
                    $sheet->setCellValue('R'.$i, $qrybk[$j]->bk_close_note);
                  }
                  $locate = DB::table('tb_booking_location')->where('bk_id','=',$qrybk[$j]->bk_id)->get();
                  $location = "";
                  $last_key = count($locate);
                  $a = 1;
                  foreach ($locate as $lo) {
                    $lo_id =$lo->location_id;
                      if ($lo->location_id == "1") {
                        $sheet->setCellValue('S'.$i, $lo->location_name);
                      }
                      else {
                        $location .=  "สถานที่ ".($lo->location_id-1)." : ".$lo->location_name;
                        if (++$a != $last_key) {
                          $location .= "\n";
                        }
                      }
                    }
                    $sheet->setCellValue('T'.$i, $location);
                    $sheet->setCellValue('U'.$i, (int)$qrybk[$j]->package_price);
                    if ($qrybk[$j]->bk_package == 'D') {
                      $max = max($hours_cal,$hours_close);
                      if ( $max < (int)$qrybk[$j]->package_mininum) {
                        $total = '=('.($max/24).'*U'.$i.')';
                      }else {
                        if (($max % 24)>0 ) {
                          $max = $max+1;
                        }
                          $total = '=('.($max/24).'*U'.$i.')';
                      }
                    }else {
                      if (max($hours_cal,$hours_close) < (int)$qrybk[$j]->package_mininum) {
                        $total = '=('.$qrybk[$j]->package_mininum.'*U'.$i.')';
                      }else {
                        if ($hours_cal >= $hours_close) {
                          $total = '=(O'.$i.'*U'.$i.')';
                        }else {
                          $total = '=(Q'.$i.'*U'.$i.')';
                        }
                      }
                    }

                    $sheet->setCellValue('V'.$i,$total);

              }
            });

        })->download('xlsx');

  }



}
