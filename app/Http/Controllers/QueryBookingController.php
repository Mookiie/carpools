<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;
use File;

class QueryBookingController extends Controller
{
    public function QueryView()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }

    }

    public function QueryWait()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'wait']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QueryApprove()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'approve']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QueryEject()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'eject']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QueryNonecar()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'nonecar']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QuerySuccess()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'success']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

      public function QueryCar()
      {
        if (session()->has('user_id'))
        {
          $username = session()->get('user');
          $emp_id = session()->get('user_id');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                          $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                               ->on("tb_employee.com_id","=","tb_department.com_id");
                                })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_id','=',$emp_id)->get();
          return view('booking.queryCar',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }
      public function QueryCarClose()
      {
        if (session()->has('user_id'))
        {
          $username = session()->get('user');
          $emp_id = session()->get('user_id');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                          $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                               ->on("tb_employee.com_id","=","tb_department.com_id");
                                })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_id','=',$emp_id)->get();
          return view('booking.queryCarClose',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }

      public function QueryCarOT()
      {
        if (session()->has('user_id'))
        {
          $username = session()->get('user');
          $emp_id = session()->get('user_id');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                          $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                               ->on("tb_employee.com_id","=","tb_department.com_id");
                                })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_id','=',$emp_id)->get();
          return view('booking.queryCarOT',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }


      public function QuerySetNonecar()
      {
        if (session()->has('user_id'))
        {
          $username = session()->get('user');
          $emp_id = session()->get('user_id');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                          $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                               ->on("tb_employee.com_id","=","tb_department.com_id");
                                })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_id','=',$emp_id)->get();
          return view('booking.queryNonecar',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }

      public function DriverView()
      {
        if (session()->has('user_id'))
        {
          $username = session()->get('user');
          $emp_id = session()->get('user_id');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                          $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                               ->on("tb_employee.com_id","=","tb_department.com_id");
                                })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_id','=',$emp_id)->get();
          return view('booking.driver',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }

    public function Approver()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();
                foreach ($users as $lv) {
                  $lv_user = $lv->emp_level;
                }
                if ($lv_user >= '1') {
                  return view('booking.queryApprove',['users'=>$users,'valuepage'=>'approve']);
                }
                else {
                    exit("<script>window.location='/dashboard';</script>");
                }
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }

    }

    public function InsertsBooking(Request $req)
    {
      $mtel = $req->input('mtel');
      $ttel = $req->input('ttel');
      $date_start = explode("/",$req->input('bk_start_date'));
      $bk_start_start = $date_start[0]."-".$date_start[1]."-".$date_start[2]." ".$req->input('bk_start_start');
      date_default_timezone_set("Asia/Bangkok");
      $date_now = date("Y/m/d H:i");
      if ($bk_start_start<$date_now) {
      if ($mtel == ""){
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์".$mtel."23");
      }
      else{
        if (!preg_match("/^\d{10}$/", $mtel)) {
            $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้องและต้องไม่มีเครื่องหมาย");
            }
        else {
              $date_end = explode("/",$req->input('bk_end_date'));
              $bk_end_start = $date_end[0]."-".$date_end[1]."-".$date_end[2]." ".$req->input('bk_end_start');
              $emp_id = $req->input('emp_id');
              $com_id = $req->input('com_id');
              $ctype = $req->input('ctype');
              $cdep = $req->input('cdep');
              $dep_id = $req->input('dep_id');
              $job_id = $req->input('job_id');
              $sitecode = $req->input("site");
              $person = $req->input('count_person');
              $obj = $req->input('bk_obj');
              $note = $req->input('bk_note');
              $cus_id = $req->input('cus_id') ;
              $cus_email = $req->input('cus_email') ;
              $cus_fname = $req->input('cus_fname') ;
              $cus_lname = $req->input('cus_lname') ;
              $cus_tel = $req->input('cus_tel') ;
              $gmail = $req->input('gmail') ;
              $bk_package = $req->input('package_type') ;
              $req_id = $req->input('req_id') ;
              $req_email = $req->input('req_email') ;
              $req_fname = $req->input('req_fname') ;
              $req_lname = $req->input('req_lname') ;
              $req_tel = $req->input('req_tel') ;
              $bk_application_no = strtoupper($req->input('bk_application_no')) ;
              if ($req->input('cus_id') == '') {
                $cus_id = $this->InsertCustomer($com_id,$cus_email,$cus_fname,$cus_lname,$cus_tel);
              }else {
                $this->UpdateCustomer($cus_id,$cus_fname,$cus_lname,$cus_tel);
              }
              if ($req->input('req_id') == '') {
                $req_id = $this->InsertRequester($com_id,$req_email,$req_fname,$req_lname,$req_tel);
              }else {
                $this->UpdateRequester($req_id,$req_fname,$req_lname,$req_tel);
              }
              if ($req->input('times') != "") {
                  $times = $req->input('times');
              }else {
                  $times = null;
              }
              if ($req->input('support_hotel') != "") {
                  $support_hotel = $req->input('support_hotel');
              }else {
                  $support_hotel = null;
              }
              if ($req->input('overtime') != "") {
                  $overtime = $req->input('overtime');
              }else {
                  $overtime = null;
              }

              $file = "";
              $date =date("Y-m-d H:i:s");

              //update tel
              $sqlUPDATE = DB::table('tb_employee')
                              ->where('emp_id','=',$emp_id)
                              ->update([
                                         'emp_tel'=>$mtel,
                                         'emp_table'=>$ttel
                                ]);

              //gen_id
              $id = "";
              $ymd = date("ym",strtotime('+543 years'));
              $sql_booking = DB::table('tb_booking')->select('bk_id')->where('bk_id','like','BK'.$ymd.'%')->where('com_id','=',$com_id)->orderBy('bk_id','desc')->limit(1)->get();
              if (count($sql_booking)<=0) {
                $id = 1;
              }
              else{
                foreach ($sql_booking as $bk):
                  $last_id = $bk->bk_id;
                endforeach;
                  $old = explode("BK",$last_id);
                  $old_ymd = substr($old[1],0,4);
                  $old_id = substr($old[1],4,6);
                  if($old_ymd == $ymd){
                    $id = $old_id+1;
                  }else{
                    $id = 1;
                  }
                }
              $bk_id = "BK".$ymd.sprintf("%06d",$id);
              // end gen_id
              //  echo $bk_id;

              $status="wait";
              $sql_user = DB::table('tb_employee_login')->where(['emp_id'=>$emp_id])->get();
              foreach ($sql_user as $user):
                $id_lv = $user->emp_level;
              endforeach;

                  if ($req->hasFile('image')) {
                    //upload file
                        foreach ($req->file('image') as $file) {
                        $filename = "doc_".time();
                        $type = $file->extension();
                        $nfile = $filename."_".rand(10,99).'.'.$type;
                        // $file = $file->storeAs('public/document',$nfile);
                        Storage::disk('uploads_document')->put($nfile, \File::get($file));
                        $no = DB::table('tb_booking_doc')->where('bk_id','=',$bk_id)->count();
                        $sqlbooking =  DB::table('tb_booking_doc')->insert([ 'bk_id' => "$bk_id",
                                                                             'no'=>($no+1),
                                                                             'bk_doc'=>"$nfile"
                                                                          ]);
                    }
                  }
                  else {
                    $nfile = "";
                  }

                    // hello mook
                    $sqlbooking =  DB::table('tb_booking')->insert(
                                [ 'bk_id' => "$bk_id",
                                  'com_id' => "$com_id",
                                  'bk_date' => "$date",
                                  'bk_start_start' => "$bk_start_start",
                                  'bk_end_start' => "$bk_end_start",
                                  'bk_use' => $times,
                                  'bk_percon' => "$person",
                                  'bk_obj' => "$obj",
                                  'bk_note' => "$note",
                                  'dep_id' => "$dep_id",
                                  'job_id' => "$job_id",
                                  'bk_sitecode'=>"$sitecode",
                                  'dep_car'=> "$cdep",
                                  'bk_mtel'=>"$mtel",
                                  'bk_ttel'=>"$ttel",
                                  'emp_id' => "$emp_id",
                                  'bk_status' => "$status",
                                  'ctype_id' => "$ctype",
                               // 'bk_doc' => "$nfile",
                                  'bk_ot'=>$overtime,
                                  'cus_id'=>$cus_id,
                                  'bk_gmail'=>$gmail,
                                  'bk_package'=>$bk_package,
                                  'req_id'=>$req_id,
                                  'bk_application_no'=>$bk_application_no,
                                  'support_hotel' =>$support_hotel
                                  // 'approve_by' =>"$emp_id",
                                  // 'approve_date'=>"$date",
                                  // 'bk_tigket'=>"$tigket"
                                ]);

                            $msg ="";
                            if (!$sqlbooking) {
                              $msg = array("bk_id"=>"","success"=>false,"msg"=>"");
                            }
                            else {
                              $msg = array("bk_id"=>"$bk_id","success"=>true,"msg"=>"");
                              session()->put('bk_id', $bk_id);

                            }

            } //else format tel
          }//else mtel ''
        }//จองก่อนเวลา
        else {
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"ไม่สามารถจองย้อนหลังได้");
        }//จองหลังเวลา

      return Response(json_encode($msg));

    }

    public function LocationInserts(Request $req)
    {
      $emp_id = $_POST['emp_id'];
      $bk_id = $_POST['id'];
      $com_id = $_POST['com_id'];
      $count_where = $_POST['count_where'];
      for($i=0;$i<$count_where;$i++){
         $where = $_POST['bk_where'.($i+1)];
            $sqlbooking_locate =DB::table('tb_booking_location')->insert(
              ['bk_id' => "$bk_id",
              'com_id' => "$com_id",
              'location_id' => ($i+1),
              'location_name' => "$where"]);
        }
        $msg ="";
        if (!$sqlbooking_locate) {
          $msg = array("bk_id"=>"","success"=>false);
        }
        else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);
          session()->put('bk_id', $bk_id);
        }
        return Response(json_encode($msg));
    }

    public function LocateDB(Request $req)
    {
      $area = array( );
      $id = $req->input('id');
      $sql = DB::table('tb_booking_location')->where("bk_id","=","$id")->get();
      foreach ($sql as $bk):
       $bk_id = $bk->bk_id;
       $location_id = $bk->location_id;
       $location_name = $bk->location_name;
       $array_locate =  array('id' => $location_id ,'name' => $location_name ,'bk_id' =>$bk_id );
      //  return Response(json_encode($array_locate));
       array_push($area,$array_locate);
      endforeach;
      // echo json_encode($area);
      return Response(json_encode($area));
    }

    public function InsertsLocation(Request $req)
    {
       $bk_id = $req->input('id');
       $location_name = $req->input('location_id');
       $latitude = $req->input('latitude');
       $longitude = $req->input('longitude');

      $sqlbooking_locate =DB::table('tb_booking_location')
                    ->where('bk_id', '=' ,$bk_id)
                    ->where('location_name', '=' ,$location_name)
                    ->update(['location_lat' => $latitude,
                              'location_lng' =>$longitude ]);

        $msg ="";
        if (!$sqlbooking_locate) {
          $msg = array("bk_id"=>"","success"=>false);
        }
        else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);
          session()->put('id', $bk_id);

        }
        return Response(json_encode($msg));
    }

    public function ApproveBooking(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
       $bk_id = $req->input("data");
       $emp_id = $req->input("emp");
       $type = $req->input("type");
       $reasons = $req->input("reasons");
       $date =date("Y-m-d H:i:s");
       if($type == 'approve'||$type == 'eject'){
         $sqlUPDATE = DB::table('tb_booking')
                    ->where('bk_id', '=' ,$bk_id)
                    ->update(['bk_status' => $type,
                              'approve_by' =>$emp_id,
                              'approve_date' => $date,
                              'bk_reasons' => $reasons,
                          ]);
        }
        elseif ($type == 'nonecar'||$type == 'success') {
          $sqlUPDATE = DB::table('tb_booking')
                     ->where('bk_id', '=' ,$bk_id)
                     ->update(['bk_status' => $type,
                               'setcar_by' =>$emp_id,
                               'setcar_date' => $date,
                               'bk_reasons' => $reasons,
                           ]);
        }

           if(!$sqlUPDATE){
             $msg = array("success"=>false,"msg"=>"");//2
           }else{
             $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id);//1
           }

          return Response(json_encode($msg));
    }

    public function ReApproveBooking(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
       $bk_id = $req->input("bk_id");
       $emp_id = $req->input("emp_id");
       $type = $req->input("type");
       $reasons = $req->input("reasons");
       $date =date("Y-m-d H:i:s");

        // $sql = DB::table('tb_booking')->where('bk_id','=',$bk_id)->get();
        // foreach ($sql as $bk) {
        //     $bk_merge = $bk->bk_merge;
        // }
        // if ($bk_merge !="") {
        //   $pos = strpos($bk_merge,';');
        //   if($pos!==FALSE){
        //      $str = explode(";",$bk_merge);
        //     }else{
        //      $str = array($bk_merge);
        //     }
        //   for ($i=0; $i < count($str); $i++) {
        //     $sqlUPDATEMerge = DB::table('tb_booking')
        //                  ->where('bk_id', '=' ,$str[$i])
        //                  ->update(['bk_status' => $type,
        //                            'resetcar_by' =>$emp_id,
        //                            'resetcar_date' => $date,
        //                            'resetcar_reasons' => $reasons,
        //                            'setcar_by' =>null,
        //                            'setcar_date'=>null,
        //                            'car_id'=>null,
        //                            'drive_id'=>null,
        //                            'bk_merge' => null
        //                        ]);
        //   }
        //
        // }

        $sqlUPDATE = DB::table('tb_booking')
                     ->where('bk_id', '=' ,$bk_id)
                     ->update(['bk_status' => $type,
                               'resetcar_by' =>$emp_id,
                               'resetcar_date' => $date,
                               'resetcar_reasons' => $reasons
                           ]);

           if(!$sqlUPDATE && !$sqlUPDATEMerge){
             $msg = array("success"=>false,"msg"=>"");//2
           }else{
             $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id);//1
           }

          return Response(json_encode($msg));
    }

    public function ApproveMailBooking()
    {
      return view('booking.approvemail');
    }

    public function EditBooking(Request $req)
    {
      $mtel = $req->input('mtel');
      $ttel = $req->input('ttel');
      $req->input('bk_start_date');
      $date_start = str_replace("/","-",$req->input('bk_start_date'));
      $bk_start_start = $date_start." ".$req->input('bk_start_start');
      // $bk_start_start = $date_start[0]."-".$date_start[1]."-".$date_start[2]." ".$req->input('bk_start_start');
      date_default_timezone_set("Asia/Bangkok");
      $date_now = date("Y/m/d H:i");
      if ($bk_start_start<$date_now) {
      if ($mtel == ""){
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์".$mtel."23");
      }
      else{
        if (!preg_match("/^\d{10}$/", $mtel)) {
            $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง".$mtel."23");
            }
        else {
              $date_end = explode("/",$req->input('bk_end_date'));
              $bk_end_start = $date_end[0]."-".$date_end[1]."-".$date_end[2]." ".$req->input('bk_end_start');
              $emp_id = $req->input('emp_id');
              $com_id = $req->input('com_id');
              $bk_id = $req->input('bk_id');
              $ctype = $req->input('ctype');
              $cdep = $req->input('cdep');
              $dep_id = $req->input('dep_id');
              $job_id = $req->input('job_id');
              $sitecode = $req->input("site");
              $person = $req->input('count_person');
              $obj = $req->input('bk_obj');
              $note = $req->input('bk_note');
              $date =date("Y-m-d H:i:s");
              $cus_id = $req->input('cus_id') ;
              $cus_email = $req->input('cus_email') ;
              $cus_fname = $req->input('cus_fname') ;
              $cus_lname = $req->input('cus_lname') ;
              $cus_tel = $req->input('cus_tel') ;
              $gmail = $req->input('gmail') ;
              $bk_package = $req->input('package_type') ;
              $req_id = $req->input('req_id') ;
              $req_email = $req->input('req_email') ;
              $req_fname = $req->input('req_fname') ;
              $req_lname = $req->input('req_lname') ;
              $req_tel = $req->input('req_tel') ;
              if ($req->input('cus_id') == '') {
                $cus_id = $this->InsertCustomer($com_id,$cus_email,$cus_fname,$cus_lname,$cus_tel);
              }else {
                $this->UpdateCustomer($cus_id,$cus_fname,$cus_lname,$cus_tel);
              }
              if ($req->input('req_id') == '') {
                $req_id = $this->InsertRequester($com_id,$req_email,$req_fname,$req_lname,$req_tel);
              }else {
                $this->UpdateRequester($req_id,$req_fname,$req_lname,$req_tel);
              }
              if ($req->input('times') != "") {
                  $times = $req->input('times');
              }else {
                  $times = null;
              }
              if ($req->input('overtime') != "") {
                  $overtime = $req->input('overtime');
              }else {
                  $overtime = null;
              }

              //update tel
              $sqlUPDATE = DB::table('tb_employee')
                              ->where('emp_id','=',$emp_id)
                              ->update([
                                         'emp_tel'=>$mtel,
                                         'emp_table'=>$ttel
                                ]);


              $sql_user = DB::table('tb_employee_login')->where(['emp_id'=>$emp_id])->get();
              foreach ($sql_user as $user):
                $id_lv = $user->emp_level;
              endforeach;

                    $sqlbooking =  DB::table('tb_booking')
                                      ->where('bk_id','=', $bk_id)
                                      ->update([ 'bk_id' => $bk_id,
                                                  'com_id' => $com_id,
                                                  'bk_start_start' => $bk_start_start,
                                                  'bk_end_start' => $bk_end_start,
                                                  'bk_use' => $times,
                                                  'bk_percon' => $person,
                                                  'bk_obj' => $obj,
                                                  'bk_note' => $note,
                                                  'bk_sitecode'=>$sitecode,
                                                  'dep_car'=> $cdep,
                                                  'bk_mtel'=>$mtel,
                                                  'bk_ttel'=>$ttel,
                                                  'ctype_id' => $ctype,
                                                  'edit_by' =>$emp_id,
                                                  'edit_date'=>$date,
                                                  'bk_ot' => $overtime,
                                                  'bk_ot'=>$overtime,
                                                  'cus_id'=>$cus_id,
                                                  'bk_gmail'=>$gmail,
                                                  'bk_package'=>$bk_package,
                                                  'req_id'=>$req_id
                                                ]);

                            $msg ="";
                            if (!$sqlbooking) {
                              $msg = array("bk_id"=>"$bk_id","success"=>false,"msg"=>"");
                            }
                            else {
                              $msg = array("bk_id"=>"$bk_id","success"=>true,"msg"=>"");
                            }


            } //else format tel
          }//else mtel ''
        }//จองก่อนเวลา
        else {
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"ไม่สามารถจองย้อนหลังได้");
        }//จองหลังเวลา

      return Response(json_encode($msg));

    }

    public function LocationEdit(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $bk_id = $req->input('id');
      $com_id = $req->input('com_id');
      $count_where = $req->input('count_where');
      $sql = DB::table('tb_booking_location')->where('bk_id','=',$bk_id)->get();
      for($i=0;$i<$count_where;$i++){
         $where = $req->input('bk_where'.($i+1));
         if ( count($sql) >= $i+1) {
           $sqlbooking_locate =DB::table('tb_booking_location')
           ->where('bk_id', "=", $bk_id)
           ->where('location_id', "=", ($i+1))
           ->update(['com_id' => "$com_id",
                     'location_name' => "$where"]);
         }
         else {
           $sqlbooking_locate =DB::table('tb_booking_location')
           ->insert(['bk_id' => "$bk_id",
                     'com_id' => "$com_id",
                     'location_id' => ($i+1),
                     'location_name' => "$where"]);
         }
        }
        if ($count_where < count($sql)) {
          for($i=0;$i<count($sql)-$count_where;$i++){
            $sqlbooking_locate =DB::table('tb_booking_location')
            ->where(['bk_id' => "$bk_id",
                      'location_id' => count($sql)-$i
                    ])
            ->delete();
          }
        }
        // $msg ="";
        // if (!$sqlbooking_locate) {
        //   $msg = array("bk_id"=>"","success"=>false);
        // }
        // else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);

        // }
        return Response(json_encode($msg));
    }

    public function EditLocation(Request $req)
    {
       $bk_id = $req->input('id');
       $location_name = $req->input('location_id');
       $latitude = $req->input('latitude');
       $longitude = $req->input('longitude');

      $sqlbooking_locate =DB::table('tb_booking_location')
                    ->where('bk_id', '=' ,$bk_id)
                    ->where('location_name', '=' ,$location_name)
                    ->update(['location_lat' => $latitude,
                              'location_lng' =>$longitude ]);
        $msg ="";
        if (!$sqlbooking_locate) {
          $msg = array("bk_id"=>"","success"=>false);
        }
        else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);
          session()->put('id', $bk_id);

        }
        return Response(json_encode($msg));
    }

    public function AddBookingOT()
    {
      if (session()->has('user_id'))
      {
        $username = session()->get('user');
        $emp_id = session()->get('user_id');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_id','=',$emp_id)->get();

        return view('booking.addOT',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function package_type(Request $req)
    {
      $ctype_id = $_POST['ctype'];
      $com_id = $_POST['com_id'];
      $package_type = $_POST['package'];
      $sqlpackage_type = DB::table('tb_package')->where('com_id','=',$com_id)->where('ctype_id','=',$ctype_id)->get();
      $select = "";
      foreach ($sqlpackage_type as $package) {
        if ($package_type == $package->package_type) {
          $select .= "<option value='".$package->package_type."' selected>".$package->package_type.". ".$package->package_detail." - ".$package->package_price."</option>";
        }
      $select .= "<option value='".$package->package_type."'>".$package->package_type.". ".$package->package_detail." - ".$package->package_price."</option>";
      // $select .= "<option value='".$package->package_type."'>".$package->package_type.". ".$package->package_detail."</option>";
      }
      $msg = array("status"=>200,"success"=>true,"msg"=> $select);
      return Response(json_encode($msg));
    }

    public function InsertCustomer($com_id,$cus_email,$cus_fname,$cus_lname,$cus_tel)
    {
      //gen_id
      $id = "";
      $ymd = date("ym",strtotime('+543 years'));
      $sql_customer = DB::table('tb_customer')->where('cus_id','like','C'.$ymd.'%')->select('cus_id')->orderBy('cus_id','desc')->limit(1)->get();
      if (count($sql_customer)<=0) {
        $id = 1;
      }
      else{
        foreach ($sql_customer as $bk):
          $last_id = $bk->cus_id;
        endforeach;
          $old = explode("C",$last_id);
          $old_ymd = substr($old[1],0,4);
          $old_id = substr($old[1],4,6);
          if($old_ymd == $ymd){
            $id = $old_id+1;
          }else{
            $id = 1;
          }
        }
        $cus_id = "C".$ymd.sprintf("%06d",$id);
        // end gen_id

        $sqlbooking =  DB::table('tb_customer')->insert(
                    [ 'cus_id' => "$cus_id",
                      'com_id' => "$com_id",
                      'email'=>"$cus_email",
                      'fname'=>"$cus_fname",
                      'lname'=>"$cus_lname",
                      'tel'=>"$cus_tel"
                    ]);
      return $cus_id;
    }
    public function searchCustomer(Request $req)
    {
      $text = $req->input('email');
      $texttel = $req->input('tel');
      $com_id = $req->input('com_id');
      $sql = DB::table('tb_customer')->where('com_id','=',$com_id)->where('email','=',$text)->orWhere('tel','=',$texttel)->get();
      if (count($sql)>0) {
        $msg = array("status"=>200,"success"=>true,"msg"=> $sql[0],"count"=>count($sql));
      }else {
        $msg = array("status"=>404,"success"=>false,"msg"=> "","count"=>count($sql));
      }
      return Response(json_encode($msg));
    }
    public function UpdateCustomer($cus_id,$cus_fname,$cus_lname,$cus_tel)
    {
      $sqlUPDATE = DB::table('tb_customer')
                      ->where('cus_id','=',$cus_id)
                      ->update([
                                 'fname'=>$cus_fname,
                                 'lname'=>$cus_lname,
                                 'tel'=>$cus_tel
                        ]);
      return $sqlUPDATE;
    }

    public function InsertRequester($com_id,$req_email,$req_fname,$req_lname,$req_tel)
    {
      //gen_id
      $id = "";
      $ymd = date("ym",strtotime('+543 years'));
      $sql_requester = DB::table('tb_requester')->where('req_id','like','R'.$ymd.'%')->select('req_id')->orderBy('req_id','desc')->limit(1)->get();
      if (count($sql_requester)<=0) {
        $id = 1;
      }
      else{
        foreach ($sql_requester as $bk):
          $last_id = $bk->req_id;
        endforeach;
          $old = explode("R",$last_id);
          $old_ymd = substr($old[1],0,4);
          $old_id = substr($old[1],4,6);
          if($old_ymd == $ymd){
            $id = $old_id+1;
          }else{
            $id = 1;
          }
        }
        $req_id = "R".$ymd.sprintf("%06d",$id);
        $sqlbooking =  DB::table('tb_requester')->insert(
                    [ 'req_id' => "$req_id",
                      'com_id' => "$com_id",
                      'email'=>"$req_email",
                      'fname'=>"$req_fname",
                      'lname'=>"$req_lname",
                      'tel'=>"$req_tel"
                    ]);
      return $req_id;
    }
    public function searchRequester(Request $req)
    {
      $text = $req->input('email');
      $texttel = $req->input('tel');
      $com_id = $req->input('com_id');

      $sql = DB::table('tb_requester')->where('com_id','=',$com_id)->where('email','=',$text)->orWhere('tel','=',$texttel)->get();
      if (count($sql)>0) {
        $msg = array("status"=>200,"success"=>true,"msg"=> $sql[0],"count"=>count($sql));
      }else {
        $msg = array("status"=>404,"success"=>false,"msg"=> "","count"=>count($sql));
      }
      return Response(json_encode($msg));
    }
    public function UpdateRequester($req_id,$req_fname,$req_lname,$req_tel)
    {
      $sqlUPDATE = DB::table('tb_requester')
                      ->where('req_id','=',$req_id)
                      ->update([
                                 'fname'=>$req_fname,
                                 'lname'=>$req_lname,
                                 'tel'=>$req_tel
                        ]);
      return $sqlUPDATE;
    }

}
