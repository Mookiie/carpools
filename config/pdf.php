<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage'
];

// return [
// 	'format'           => 'A4', // See https://mpdf.github.io/paging/page-size-orientation.html
// 	'author'           => 'John Doe',
// 	'subject'          => 'This Document will explain the whole universe.',
// 	'keywords'         => 'PDF, Laravel, Package, Peace', // Separate values with comma
// 	'creator'          => 'Laravel Pdf',
// 	'display_mode'     => 'fullpage'
// ];

// return [
// 	'format'           => 'A4',
// 	'author'           => 'Carpool Siamraj',
// 	'subject'          => '',
// 	'keywords'         => '', // Separate values with comma
// 	'creator'          => 'Carpool Siamraj',
// 	'display_mode'     => 'fullpage',
//
// 	'font_path' => asset("fonts/"),
// 	'font_data' => [
// 		'THSaraban' => [
// 			'R'  => 'THSarabunNew.ttf',    // regular font
// 			'B'  => 'THSarabunNew Bold.ttf',       // optional: bold font
// 			'I'  => 'THSarabunNew Italic.ttf',     // optional: italic font
// 			'BI' => 'THSarabunNew BoldItalic.ttf' // optional: bold-italic font
// 			//'useOTL' => 0xFF,    // required for complicated langs like Persian, Arabic and Chinese
// 			//'useKashida' => 75,  // required for complicated langs like Persian, Arabic and Chinese
// 		]
// 		// ...add as many as you want.
// 	]
// ];
