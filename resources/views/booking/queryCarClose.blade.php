@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

<?php
          foreach ($users as $u):
           $emp_lv = $u->emp_level;
           $emp_id = $u->emp_id;
           $com_id = $u->com_id;
           $job_id = $u->job_id;
          endforeach;
 ?>
 <div class="container-dashboard ">
   <input id="com_id" name="com_id" type="hidden" class="form-control" value="{{$com_id}}">
   <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">
      <div class="card col-md-12 row">
      <div class="card-header">
        <span class="fa fa-archive"> รายการรอปิดงาน</span>
      </div>

      <div class="card-block">
      <div id="tblBk" class="table-responsive" Data-id="emp=<?php echo $emp_id;?>">
      <table class="table table-bordered tblBk">
        <thead>

          <th>เลขที่อ้างอิง</th>
          <th>ผู้บันทึก</th>
          {{-- <th>แผนก</th> --}}
          <th>วันที่ขอใช้รถ</th>
          <th>ประเภทรถ</th>
          {{-- <th>หมายเหตุ</th> --}}
          <th></th>
        </thead>
        <tbody class="">
          <?php
            date_default_timezone_set("Asia/Bangkok");
            $date_now = date("Y/m/d");
            $sqlbooking_sql = DB::table('tb_booking')
                          ->join("tb_car_type",function($join){
                                $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                                $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                            })
                          ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                          ->where('tb_booking.bk_status', '=', 'success')
                          ->where('send_satisfaction','=','0')
                          ->where('tb_booking.com_id','=',$com_id);
            if ($emp_lv<9) {
              $sqlbooking = $sqlbooking_sql->where('tb_booking.dep_car', '=', $job_id)->where('tb_booking.bk_ot','=',null )
              ->orderBy('bk_end_start')->get();
            }
            else {
              $sqlbooking = $sqlbooking_sql->orderBy('bk_end_start')->get();
            }

            $numbooking = count($sqlbooking);
        if($numbooking > 0){

        ?>
              @foreach ($sqlbooking as $bc)
        <?php
              $d1= new DateTime($bc->bk_start_start);
              $d2 = new DateTime(date("Y-m-d"));
              $diff = $d1->diff($d2)->format("%a");

              if($numbooking > 0){
                // if ($bc->approve_date > date("Y-m-d 15:30:00")) {
                //   echo "<tr bgcolor='#ce93d8'>";
                // }
                // if ($bc->bk_date < date("Y-m-d 00:00:00", strtotime("+1 days"))) {
                //   if ($diff==0) {
                //       echo "<tr>";
                //   }elseif ($diff==1) {
                //       echo "<tr bgcolor='#fce4ec'>";
                //   }elseif ($diff==2) {
                //       echo "<tr bgcolor='#fff8e1'>";
                //   }elseif ($diff==3) {
                //       echo "<tr bgcolor='#e8f5e9'>";
                //   }else {
                //     echo "<tr bgcolor='#ccc'>";
                //   }
                // }else {
                //   echo "<tr bgcolor='#ccc'>";
                // }

            ?>
            <tr>
                <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">
                  <?php echo $bc->bk_id ?>
                </td>
                <td class="detailBk text-black fsize" data-id="bk=<?php echo $bc->bk_id;?>">
                  <?php echo $bc->emp_fname." ".$bc->emp_lname; ?>
                </td>
                <td class="detailBk text-black" width="40%" data-id="bk=<?php echo $bc->bk_id;?>">
                  <?php  if (!$bc->bk_use) {
                    echo datetime($bc->bk_start_start);echo " ถึง ";datetime($bc->bk_end_start);
                  } else {
                    echo datetimesome($bc->bk_start_start,$bc->bk_end_start);
                  }

                  $locate = DB::table('tb_booking_location')->where('bk_id','=',$bc->bk_id)->get();
                  foreach ($locate as $lo) {
                    $lo_id =$lo->location_id;
                      if ($lo->location_id == "1") {
                        echo "<br /><b>สถานที่เริ่มต้น</b> : ".$lo->location_name;
                      }
                      else {
                        echo "<br /><b>สถานที่ ".($lo->location_id-1)."</b> : ".$lo->location_name;
                      }
                    }
                   ?>
                   <br />
                   <b>วัตถุประสงค์</b> :
                   <?php
                         if ($bc->bk_obj == "") {
                           echo "-";
                         }else {
                           echo $bc->bk_obj ;
                         }
                   ?>
                   <br>
                   <b>หมายเหตุ</b> :
                   <?php
                         if ($bc->bk_note == "") {
                           echo "-";
                         }else {
                           echo $bc->bk_note ;
                         }
                   ?>
                   <br>
                   <b>ข้อมูลผู้ขอ</b> :
                   <?php
                      $sql_requester = DB::table('tb_requester')->where('req_id','=',$bc->req_id)->get();
                    ?>
                    @foreach ($sql_requester as $req)
                        {{$req->fname." ".$req->lname." (".$req->tel.")"}}
                    @endforeach
                   <br>
                   <b>ข้อมูลผู้ใช้</b> :
                   <?php
                      $sql_customer = DB::table('tb_customer')->where('cus_id','=',$bc->cus_id)->get();
                    ?>
                    @foreach ($sql_customer as $cus)
                        {{$cus->fname." ".$cus->lname." (".$cus->tel.")"}}
                    @endforeach
                   <br>
                   <b>จำนวนผู้เดินทาง</b> :{{$bc->bk_percon}} คน

                </td>
                <td class="detailBk text-black" width="10%" data-id="bk=<?php echo $bc->bk_id;?>">
                  {{$bc->ctype_name}}
                  <br>
                  <?php  $car = DB::table('tb_car')->where('car_id','=',$bc->car_id)->where('com_id','=',$com_id)->select('car_number')->get(); ?>
                  @foreach ($car as $cr)
                    {{'ทะเบียน : '.$cr->car_number}}
                  @endforeach
                  <br>
                  <?php  $driver = DB::table('tb_driver')->where('drive_id','=',$bc->drive_id)->where('com_id','=',$com_id)->select('drive_fname','drive_lname')->get(); ?>
                  @foreach ($driver as $dr)
                    {{'คนขับ : '.$dr->drive_fname.' '.$dr->drive_lname}}
                  @endforeach

                </td>

                <td align="center">
                  <button class="btn btn-sm btn-primary setClose" data-id="bk=<?php echo $bc->bk_id; ?>">
                    <span class="fa fa-sign-in"> ปิดงาน</span>
                  </button>
                  <button class="btn btn-sm btn-danger setcloseeject" data-id="bk={{$bc->bk_id}}">
                    <span class="fa fa-minus-circle"></span> ยกเลิก
                  </button>
                </td>
              </tr>
              <?php }
               elseif ($numbooking = 0){ ?>
                 <tr>
                     <td colspan="7" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                 </tr>
               <?php }?>
       @endforeach

      <?php }else{  ?>
                  <tr>
                      <td colspan="7" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                  </tr>
      <?php } ?>
        </tbody>
      </table>
      </div>
      <div id="calendarBk"></div>

    </div>
    </div>
  </div>
    <div class="modal-area"></div>
    <?php
    function datetime($datetime)
    {
      $y = substr($datetime,0,4);
      $m = substr($datetime,5,2);
      $d = substr($datetime,8,2);
      $h = substr($datetime,11,2);
      $i = substr($datetime,14,2);
      echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
    }
    function datetimesome($datetime1,$datetime2)
    {
      $y1 = substr($datetime1,0,4);
      $m1 = substr($datetime1,5,2);
      $d1 = substr($datetime1,8,2);
      $h1 = substr($datetime1,11,2);
      $i1 = substr($datetime1,14,2);
      $y2 = substr($datetime2,0,4);
      $m2 = substr($datetime2,5,2);
      $d2 = substr($datetime2,8,2);
      $h2 = substr($datetime2,11,2);
      $i2 = substr($datetime2,14,2);
      return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
    }
     ?>
    <script>
      $(".detailBk").click(function(){
        var detail = $(this).data("id");
       $.ajax({
                url:"/detail",
                data:detail,
                type:"GET",
                success:function(data){
                  // console.log(data);
                $(".modal-area").html(data);
                $("#modalBk").modal("show");
              }
          });
      });

      // $(".MergeCar").click(function(){
      //   var detail = $(this).data("id");
      //  $.ajax({
      //           url:"/merge",
      //           data:detail,
      //           type:"POST",
      //           success:function(data){
      //             // console.log(data);
      //           $(".modal-area").html(data);
      //           $("#modalBk").modal("show");
      //         }
      //     });
      // });

      // $(".setCar").click(function(){
      //   var sData = $(this).data("id");
      //   $.ajax({
      //     url:"/setcar",data:sData,type:"GET",success:function(data){
      //         $(".modal-area").html(data);
      //         $("#modalBk").modal("show");
      //         var ctype = $('#carType').val();
      //         var com_id = $('#com_id').val();
      //         $.ajax({
      //           type:'POST',
      //           data: sData+'&ctype='+ctype+'&com_id='+com_id,
      //           url:'/tbsetcar',
      //           success:function(data){
      //             $('#TbSetCar').html(data)
      //             $(".setCar").click(function(){
      //               car = $(this).data("id");
      //               showandhide(2,1,3);
      //               $("#StepProgress").css({"transition":"1s","width":"50%"});
      //             });
      //
      //           }
      //         })
      //
      //       $('#carType').change(function () {
      //         var ctype = $('#carType').val();
      //         var com_id = $('#com_id').val();
      //         $.ajax({
      //           type:'POST',
      //           data: sData+'&ctype='+ctype+'&com_id='+com_id,
      //           url:'/tbsetcar',
      //           success:function(data) {
      //             $('#TbSetCar').html(data)
      //             $(".setCar").click(function(){
      //               car = $(this).data("id");
      //               showandhide(2,1,3);
      //               $("#StepProgress").css({"transition":"1s","width":"50%"});
      //             });
      //
      //           }
      //         })
      //       })
      //
      //     }
      //   });
      // });

      $(".setClose").click(function() {
          var id = $(this).data("id")
          var emp_id = $("#emp_id").val();
          var com_id = $("#com_id").val();
          $.ajax({
            url:"/setclose",data:id+"&emp_id="+emp_id+"&com_id="+com_id,type:"POST",success:function(data){
              $(".modal-area").html(data);
              $("#modalBk").modal("show");
            }
          })
      })
      $(".setcloseeject").click(function() {
          var id = $(this).data("id")
          var emp_id = $("#emp_id").val();
          var com_id = $("#com_id").val();
          $.ajax({
            url:"/setcloseeject",data:id+"&emp_id="+emp_id+"&com_id="+com_id,type:"POST",success:function(data){
              $(".modal-area").html(data);
              $("#modalBk").modal("show");
            }
          })
      })


    </script>
@endsection
