<div class="form-group row {{$time}}">
  <label for="tigket" class="col-md-2 col-form-label">{{"คันที่".substr($round,2,1)}}</label>
  <div class="col-md-3">
    <select class="form-control" id="{{"car".$round}}" name="{{"car".$round}}">
      <option value="" disabled>เลือกทะเบียนรถ</option>
      <?php
        $sql_car = DB::table('tb_car')->where('com_id','=',$com_id)->where('dep_id','=',$job_id)->whereIN('car_status',['4','0'])->get();
        foreach ($sql_car as $car) {
          echo "<option value='".$car->car_id."'>".$car->car_number."</option>";
        }
       ?>
    </select>
  </div>
  <div class="col-md-4">
    <select class="form-control" id="{{"driver".$round}}" name="{{"driver".$round}}">
      <option value="" disabled>เลือกคนขับ</option>
      <?php
        $sql_driver = DB::table('tb_driver')->where('com_id','=',$com_id)->where('dep_id','=',$job_id)->where('drive_status','=','0')->get();
        foreach ($sql_driver as $driver) {
            echo " <option value='".$driver->drive_id."'>".$driver->drive_fname." ".$driver->drive_lname."</option>";
        }
       ?>
    </select>
  </div>
</div>
