<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลหน่วยงาน</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>" >
           <!-- รหัสพนักงาน -->
            <?php
            // dep_id
            $sqldep = DB::table('tb_department')->where("dep_id", "=" ,$dep_id)->get();
            foreach ($sqldep as $dep):
             $name = $dep->dep_name;
             $tel = $dep->dep_tel;
            endforeach;
             ?>
            <!-- รหัสหน่วยงาน -->
          <label for="dep_id" class ="col-md-4 col-form-label text-black">รหัสหน่วยงาน</label>
              <input type="text"  id="dep_id" name="dep_id" value="<?php echo $dep_id ?>" readonly>
          </div><!-- รหัสหน่วยงาน -->

          <div class="form-group row"><!-- ชื่อหน่วยงาน -->
            <label for="dep_name" class ="col-md-4 col-form-label text-black">ชื่อหน่วยงาน</label>
            <input type="text" class="form-control col-md-6" id="dep_name" name="dep_name" value="<?php echo $name ?>">
          </div><!-- ชื่อหน่วยงาน -->

          <div class="form-group row"><!-- เบอร์โทร -->
            <label for="tel" class ="col-md-4 col-form-label text-black">เบอร์โทร</label>
            <input type="text" class="form-control col-md-6" id="tel" name="tel" value="<?php echo $tel ?>" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            <div hidden="true" id="fbtel" class="offset-md-4 form-control-feedback"><br></div>
          </div><!-- เบอร์โทร -->
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#dep_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    EditDep();
 }
});
$(".btn-create").click(function(){
  EditDep();
});

function EditDep(){
var form_create= $("#create-form").serialize();
//  console.log(form_create);
 $.ajax({
  url:"/updatedep",
  data:form_create,
  type:"POST",
  success:function(data){
      var obj =JSON.parse(data);
      // console.log(obj);
    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลหน่วยงานสำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/editdep";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
  }
 });
}
function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

 </script>
