<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลประเภทรถ</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
           <!-- รหัสพนักงาน -->
            <?php
            $sqlemp = DB::table('tb_employee')->where('emp_id','=',$emp_id)->get();
            foreach ($sqlemp as $emp) {
              $com_id = $emp->com_id;
            }
            // dep_id
            $sqlctype = DB::table('tb_car_type')->where('com_id','=',$com_id)->orderBy('ctype_id','decs')->limit(1)->get();
            foreach ($sqlctype as $ctype):
             $id = $ctype->ctype_id;
            endforeach;
             ?>

            <!-- รหัสบริษัท -->
              <input type="hidden" name="com_id" id="com_id" value="{{$com_id}}">
            <!-- รหัสบริษัท -->

            <!-- รหัสประเภทรถ -->
              <input type="hidden"  id="ctype_id" name="ctype_id" value="<?php echo $id+1 ?>">
          </div><!-- รหัสประเภทรถ -->

          <div class="form-group row"><!-- ชื่อประเภทรถ -->
            <label for="ctype_name" class ="col-md-4 col-form-label text-black">ชื่อประเภทรถ</label>
            <input type="text" class="form-control col-md-6" id="ctype_name" name="ctype_name" placeholder="ชื่อประเภทรถ">
          </div><!-- ชื่อประเภทรถ -->

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#dep_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    createctype();
 }
});
$(".btn-create").click(function(){
  createctype();
});

function createctype(){
var form_create= $("#create-form").serialize();
 $.ajax({
   url:"/createctype",
   data:form_create,
   type:"POST",
   success:function(data){
      var obj =JSON.parse(data);
    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลประเภทรถยนต์สำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/otheradd";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
   }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
