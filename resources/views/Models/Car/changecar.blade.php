@extends('welcome')
@section('content')
@include('dashboard.SideNav')
<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<div class="container" style="margin-top: 5%;">

 <div class="card-block">

     <form id="frm_ChangeCar">

       @foreach ($users as $r)
         <?php
               $emp_id=$r->emp_id;
               $lv_user = $r->emp_level;
               $dep_id = $r->dep_id;
               $com_id = $r->com_id;
               $job_id = $r->job_id;

          ?>
       @endforeach
       <?php
       $bk_id = base64_decode(base64_decode(base64_decode($bk_id)));
       $sql_bk = DB::table("tb_booking")
        ->join("tb_car",function($join){
               $join->on("tb_booking.car_id","=","tb_car.car_id")
                    ->on("tb_booking.com_id","=","tb_car.com_id");
              })
        ->join("tb_driver",function($join){
               $join->on("tb_booking.drive_id","=","tb_driver.drive_id")
                    ->on("tb_booking.com_id","=","tb_driver.com_id");
              })
        ->where("bk_id",'=',$bk_id)->get();
        foreach ($sql_bk as $bk) {
          $car_number = $bk->car_number;
          $car_use = $bk->car_id;
          $drive_id = $bk->drive_id;
          $drive_fname = $bk->drive_fname;
          $drive_lname = $bk->drive_lname;
        }
       $sql_car = DB::table("tb_car")->where("com_id",'=',$com_id)->where("dep_id",'=',$job_id)->get();
       $sql_driver = DB::table("tb_driver")->where("com_id",'=',$com_id)->where("dep_id",'=',$job_id)->orWhere("dep_id",'=','ALL')->get();
        ?>
           <div class="card offset-sm-2 col-md-8">
             <div class="card-block">
               <div class="modal-header" style="line-height: 1.6;">
                   <h6 class="modal-title"><span class="fa fa-edit text-black">  สลับการใช้งานรถยนต์ :</span>
               </div>
               <br />
               <input type="hidden"  id="bk_id" value="{{$bk_id}}">
               <input type="hidden"  id="com_id" value="{{$com_id}}">

               <div class="form-group row">
                 <label for="emp_sex" class="col-md-3 col-form-label">รถทะเบียน(เดิม)</label>
                 <div class="col-8">
                   <input type="text" id="oldcar" name="oldcar" class="form-control" value="{{$car_number}}" readonly>
                   <input type="hidden" id="oldcarid" name="oldcarid" class="form-control" value="{{$car_use}}" readonly>
                   <input type="hidden" class="form-control" name="emp_id" value="{{$emp_id}}" readonly>
                   <input type="hidden"  id="bk_id" name="bk_id" value="{{$bk_id}}">
                   <input type="hidden"  id="com_id"  name="com_id" value="{{$com_id}}">
                 </div>
               </div>

               <div class="form-group row">
                 <label for="emp_sex" class="col-md-3 col-form-label">ทะเบียนรถ(เปลี่ยน)</label>
                 <div class="col-8">
                 <select class="form-control mr-sm-2"  id="newcar" name="newcar">
                   <option value="">กรุณาเลือกรถยนต์ที่เปลื่ยน</option>
                   @foreach ($sql_car as $car)
                       <option value="{{$car->car_id}}">{{$car->car_number}}</option>
                   @endforeach
                 </select>
               </div>
               </div>

               <div class="form-group row">
                 <label for="emp_sex" class="col-md-3 col-form-label">คนขับรถ(เดิม)</label>
                 <div class="col-8">
                   <input type="text" id="olddrive" name="olddrive" class="form-control" value="{{$drive_fname}} {{$drive_lname}}" readonly>
                   <input type="hidden" id="olddriveid" name="olddriveid" class="form-control" value="{{$drive_id}}" readonly>
                 </div>
               </div>

               <div class="form-group row">
                 <label for="emp_sex" class="col-md-3 col-form-label">คนขับรถ(เปลี่ยน)</label>
                 <div class="col-8">
                 <select class="form-control mr-sm-2"  id="newdrive" name="newdrive">
                   <option value="">กรุณาเลือกคนขับรถที่เปลื่ยน</option>
                   @foreach ($sql_driver as $dv)
                       <option value="{{$dv->drive_id}}">{{$dv->drive_fname}} {{$dv->drive_lname}}</option>
                   @endforeach
                 </select>
               </div>
               </div>


               <div class="form-group row" id="groupDate1">
                   <label for="date" class ="col-md-3 col-form-label ">ใช้ถึงวันที่<label class="text-red">*</label></label>
                   <div class="col-8">
                   <input id="date" name="date" type="text" class="form-control col-md-8 bkDate" value="{{date("Y-m-d")}}" placeholder="{{date("Y-m-d")}}">
                 </div>
               </div>


                <div class="form-group row">
                  <label for="emp_fname" class="col-md-3 col-form-label">รายละเอียด<label class="text-red">*</label></label>
                   <div class="col-md-5">
                     <textarea name="detail" rows="8" cols="50"></textarea>
                   </div>
                </div>

                <div class="col-12" align="center">
                  <button type="button" class="btn btn-success" id="btn_save">บันทึก</button>
                  <button type="reset" class="btn btn-danger" id="btn_prev">ยกเลิก</button>
                </div>
             </div>
            </div>
     </form>
   </div>

</div>
<script type="text/javascript">
  $("#btn_prev").click(function () {
    window.history.back();
  })
  $("#btn_save").click(function () {
    var form_data = $("#frm_ChangeCar").serialize();
        console.log(form_data);
    $.ajax({
      url:"/ChangeCarDB",
      data:form_data,
      type:"POST",
      success:function(data){
        var obj = JSON.parse(data);
          if (obj['success']==true) {
            swal({
              type:"success",
              title:"สำเร็จ",
              text:"คุณทำการบันทึกสำเร็จ",
              confirmButtonText:"ตกลง",
              confirmButtonColor:"#2ECC71",
              closeOnConfirm:true,
            },function(isConfirm){
              window.history.back();
            });
          }
      }
    })
  })

  jQuery(".bkDate").datetimepicker({
          format:"Y-m-d",
          lang:"th",
          // minDate:'-1969/12/30',
          // minDate:'-1970/01/01',
          timepicker:false,
          scrollInput:false
  });
</script>
@endsection
