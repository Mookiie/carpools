<?php          ?>
          <div class="card col-md-12 row">
          <div class="row">
              <div class="col-md-12">
              {{-- <div class="card col-md-12"> --}}
                <div class="card-header">
                  <button class="close" id="btnClose">&times;</button>
                  <b>บันทึกข้อมูลรถยนต์ </b>
                  <input type="hidden" id="emp_id" value="data=<?php echo $emp_id; ?>">
                </div>
                <div class="card-block" id="addcar">
                  <div class="row">
                    <div class="offset-md-1 col-md-10">
                      <form name="createcarform" id="createcarform">

                         <div class="form-group row">
                            <!-- รหัสพนักงาน -->
                             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
                             <input type="hidden" name="com_id" id="com_id" value="<?php echo $com_id ?>">
                             <input type="hidden" name="dep_id" id="dep_id" value="<?php echo $dep_id ?>">

                           <!-- รหัสพนักงาน -->
                            <?php
                            // car_id
                            $sqlcar = DB::table('tb_car')->where('com_id','=',$com_id)->orderBy('car_id','decs')->limit(1)->get();
                            foreach ($sqlcar as $car):
                             $id = $car->car_id;
                            endforeach;
                            $car_id = substr($id, 0, 1);//C
                            $car_id.=date('y')+43;//60
                            $id1= substr($id, 3, 5)+1;//00001
                             ?>
                            <!-- รหัสรถยนต์ -->
                              <input type="hidden"  id="car_id" name="car_id" value="<?php echo $car_id.sprintf("%05d",$id1) ?>">
                          </div><!-- รหัสรถยนต์ -->

                          <div class="form-group row"><!-- ยี่ห้อรถยนต์ -->
                            <label for="brand_id" class="col-md-2 col-form-label ">ยี่ห้อรถ </label>
                            <div class=" col-md-9">
                              <select id="brand_id" name="brand_id" class="form-control">
                                <option value="0" selected disabled>เลือกยี่ห้อรถ</option>
                                <?php
                                    $sqlbrand_id = DB::table('tb_brand')->get();
                                        foreach ($sqlbrand_id as $brand):
                                  ?>
                                    <option value="<?php echo $brand->brand_id ?>"><?php echo $brand->brand_name ?></option>
                                  <?php endforeach; ?>
                              </select>
                            </div>
                          </div>  <!-- ยี่ห้อรถยนต์ -->

                          <div class="form-group row"><!-- รุ่น -->
                            <label for="car_model" class ="col-md-2 col-form-label ">รุ่น</label>
                            <div class="input-group col-9">
                            <input type="text" class="form-control" id="car_model" name="car_model" placeholder="รุ่นรถยนต์">
                            </div>
                          </div><!-- รุ่น -->

                          <div class="form-group row"><!-- เลขทะเบียน -->
                            <label for="car_number" class ="col-md-2 col-form-label ">ทะเบียน</label>
                            <div class="input-group col-9">
                            <input type="text" class="form-control " id="car_number" name="car_number" placeholder="หมายเลขทะเบียน">
                            </div>
                          </div><!-- เลขทะเบียน -->

                          {{-- <div class="form-group row"><!-- จังหวัด -->
                            <label for="car_province" class ="col-md-2 col-form-label ">จังหวัด</label>
                            <div class="input-group col-9">
                            <input type="text" class="form-control " id="car_province" name="car_province" placeholder="จังหวัด">
                            </div>
                          </div><!-- จังหวัด --> --}}

                          {{-- <div class="form-group row"><!-- เลขเครื่อง -->
                            <label for="machine_id" class ="col-md-2 col-form-label ">หมายเลขเครื่อง</label>
                            <div class="input-group col-9">
                              <input type="text" class="form-control" id="machine_id" name="machine_id" placeholder="หมายเลขเครื่อง">
                            </div>
                          </div><!-- เลขเครื่อง --> --}}

                          {{-- <div class="form-group row"><!-- เลขตัวถัง -->
                            <label for="body_id" class ="col-md-2 col-form-label ">หมายเลขตัวถัง</label>
                            <div class="input-group col-9">
                              <input type="text" class="form-control " id="body_id" name="body_id" placeholder="หมายเลขตัวถัง">
                            </div>
                          </div><!-- เลขตัวถัง --> --}}

                          {{-- <div class="form-group row">
                            <label for="car_price" class="col-md-2 col-form-label">ราคา </label>
                                <div class="input-group col-9" >
                                  <input id="car_price" name="car_price" type="text" class="form-control" placeholder="ราคา">
                                 <span class="input-group-addon" id="btnGroupAddon">บาท</span>
                               </div>
                        </div> --}}


                          <div class="form-group row">
                              <label for="ctype" class="col-md-2 col-form-label ">ประเภทรถ</label>
                              <div class="col-md-9">
                                <select id="ctype" name="ctype" class="form-control">
                                  <option value="0" selected disabled>เลือกประเภทรถ</option>
                                  <?php
                                      $sqlcar_type = DB::table('tb_car_type')->where('com_id','=',$com_id)->get();
                                          foreach ($sqlcar_type as $type):
                                    ?>
                                      <option value="<?php echo $type->ctype_id ?>"><?php echo $type->ctype_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                              </div>


                          </div>

                          <div class="form-group row">
                              {{-- <label for="fuel_id" class="col-md-2 col-form-label ">ประเภทน้ำมัน </label>
                              <div class="col-md-4">
                                <select id="fuel_id" name="fuel_id" class="form-control">
                                  <option value="0" selected disabled>เลือกประเภทน้ำมัน</option> --}}
                                  <?php
                                      // $sqlfuel_id = DB::table('tb_fuel')->get();
                                      //     foreach ($sqlfuel_id as $fuel):
                                    ?>
                                      {{-- <option value="{{$fuel->fuel_id}}">{{$fuel->fuel_name}}</option> --}}
                                    <?php //endforeach; ?>
                                {{-- </select>
                              </div> --}}

                              <label for="color_id" class=" col-md-2 col-form-label">สีรถ</label>
                              <div class="col-md-9">
                                <select id="color_id" name="color_id" class="form-control">
                                  <?php
                                      $sqlcolor_id = DB::table('tb_color')->get();
                                          foreach ($sqlcolor_id as $color):
                                    ?>
                                      <option value="<?php echo $color->color_id ?>"><?php echo $color->color_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                              </div>
                          </div>
                          <div class="form-group row"> <!-- สถานะ -->
                              <label for="car_status" class="col-md-2 col-form-label ">สถานะ </label>
                              <div class="col-9" >
                                <select id="car_status" name="car_status" class="form-control">
                                      <option value="0">ใช้งาน</option>
                                      <option value="1">ยกเลิกใช้งาน</option>
                                      <option value="2">ซ่อม</option>
                                </select>
                              </div>
                          </div><!-- สถานะ -->

                          {{-- <div class="form-group row"> <!-- วันที่รับเข้า -->
                              <label for="in_date" class ="col-md-2 col-form-label ">วันที่รับเข้า</label>
                              <div class="col-md-3">
                                <input id="in_date" name="in_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                            <label for="buy_date" class ="offset-md-1 col-md-2 col-form-label ">วันที่ซื้อ</label>
                            <div class="col-md-3">
                              <input id="buy_date" name="buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                            </div>
                          </div><!-- วันที่ซื้อ --> --}}

                          {{-- <div class="form-group row"> <!-- วันที่จดทะเบียน -->
                              <label for="machine_id" class ="col-md-2 col-form-label ">วันที่จดทะเบียน</label>
                              <div class="col-md-3">
                                <input id="car_buy_date" name="car_buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                              <label for="machine_id" class ="offset-md-1 col-md-2 col-form-label ">ทะเบียนหมดอายุ</label>
                              <div class="col-md-3">
                                <input id="car_buy_date" name="car_buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                          </div><!-- วันที่จดทะเบียน --> --}}

                          {{-- <div class="form-group row"> <!-- วันที่รับเข้า -->
                              <label for="machine_id" class ="col-md-2 col-form-label ">วันทำ พ.ร.บ.</label>
                              <div class="col-md-3">
                                <input id="car_buy_date" name="car_buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                              <label for="machine_id" class ="offset-md-1 col-md-2 col-form-label ">พ.ร.บ. หมดอายุ</label>
                              <div class="col-md-3">
                                <input id="car_buy_date" name="car_buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                          </div><!-- วันที่ซื้อ --> --}}

                          {{-- <div class="form-group row">
                              <label for="brand_id" class="col-md-2 col-form-label ">บริษัทประกันภัย</label>
                              <div class=" col-9" >
                                <select id="brand_id" name="brand_id" class="form-control">
                                  <option value="0">บ.</option>
                                  <option value="1">บ.</option>
                                </select>
                              </div>
                          </div> --}}

                          {{-- <div class="form-group row"> <!-- วันที่รับเข้า -->
                              <label for="machine_id" class ="col-md-2 col-form-label ">วันทำประกันภัย</label>
                              <div class="col-md-3">
                                <input id="car_buy_date" name="car_buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                              <label for="machine_id" class ="offset-md-1 col-md-2 col-form-label ">ประกันภัยหมดอายุ</label>
                              <div class="col-md-3">
                                <input id="car_buy_date" name="car_buy_date" type="text" class="form-control Date" placeholder="วว/ดด/ปี" value="" >
                              </div>
                          </div><!-- วันที่ซื้อ --> --}}

                          <div class="form-group row">
                              <label for="car_note" class="col-md-2 col-form-label">หมายเหตุ </label>
                              <div class="col-9">
                              <textarea id="car_note" name="car_note" class="form-control" rows="5"></textarea>
                              </div>
                          </div>

                          <div class="form-group row">
                            <label for="count_person" class="col-md-2 col-form-label">Image หน้า</label>
                            <input class="col-9" type="file" id="car_img_front" name="car_img_front">
                          </div>

                          <div class="form-group row">
                            <label for="count_person" class="col-md-2 col-form-label">Image ซ้าย</label>
                            <input class="col-9" type="file" id="car_img_left" name="car_img_left">
                          </div>

                          <div class="form-group row">
                            <label for="count_person" class="col-md-2 col-form-label">Image ขวา</label>
                            <input class="col-9" type="file" id="car_img_right" name="car_img_right">
                          </div>

                          <div class="form-group row">
                            <label for="count_person" class="col-md-2 col-form-label">Image หลัง</label>
                            <input class="col-9" type="file" id="car_img_back" name="car_img_back">
                          </div>

                          <div align="center">
                            <button type="submit" class="btn btn-primary" id="btn-create">บันทึก</button>
                            <button type="button" class="btn btn-secondary" id="btn-addexit">ยกเลิก</button>
                          </div>

                      </form>

                    </div>
                  </div>
                </div>

              {{-- </div> --}}
              </div>
          </div>

          </div>

          <script>
          jQuery(".Date").datetimepicker({
                  format:"d/m/Y",
                  lang:"th",
                  timepicker:false,
                  scrollInput:false
          });

          </script>
