<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
      <?php
      date_default_timezone_set("Asia/Bangkok");
      $sqlbooking_sql = DB::table('tb_booking')
                    ->leftJoin("tb_car_type",function($join){
                          $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                          $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                      })
                    ->leftJoin("tb_car",function($join){
                          $join->on("tb_booking.car_id","=","tb_car.car_id");
                          $join->on("tb_booking.com_id","=","tb_car.com_id");
                      })
                    ->leftJoin("tb_package",function($join){
                          $join->on("tb_booking.ctype_id","=","tb_package.ctype_id");
                          $join->on("tb_booking.bk_package","=","tb_package.package_type");
                          $join->on("tb_booking.com_id","=","tb_package.com_id");
                      })
                    ->leftJoin("tb_brand",function($join){
                          $join->on("tb_car.brand_id","=","tb_brand.brand_id");
                      })
                    ->leftJoin('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                    ->where('tb_booking.bk_id', '=', $bk_id)
                    ->where('tb_booking.com_id','=', $com_id)
                    ->get();
                    foreach ($sqlbooking_sql as $bk) {
                      $ctype= $bk->ctype_name;
                      $brand= $bk->brand_name;
                      $car_model= $bk->car_model;
                      $car_number= $bk->car_number;
                      $bk_start_start = $bk->bk_start_start;
                      $bk_end_start = $bk->bk_end_start;
                      $bk_application_no = $bk->bk_application_no;
                      $bk_package = $bk->bk_package;
                      $package_price = $bk->package_price;
                      $car_id = $bk->car_id;
                      $drive_id = $bk->drive_id;
                      $package_detail = $bk->package_detail;
                      $price_minimum = $bk->price_minimum;
                      $package_price = $bk->package_price;
                    }
                    $datetime_start = new DateTime($bk_start_start);
                    $date_start = $datetime_start->format('Y/m/d');
                    $time_start = $datetime_start->format('H:i');
                    $datetime_end = new DateTime($bk_end_start);
                    $date_end = $datetime_end->format('Y/m/d');
                    $time_end = $datetime_end->format('H:i');
      ?>
            <div class="modal-header">
                <h5 class="modal-title fa fa-plus text-black"> บันทึกวันเวลายกเลิก {{$bk_id}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form id="frm_close" method="post">
                 <div class="form-group row">
                    <!-- รหัสพนักงาน -->
                     <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
                     <input type="hidden" name="bk_id" id="bk_id" value="<?php echo $bk_id ?>">
                     <input type="hidden" name="car_id" id="car_id" value="<?php echo $car_id ?>">
                     <input type="hidden" name="drive_id" id="drive_id" value="<?php echo $drive_id ?>">
                     <input type="hidden" name="com_id" id="com_id" value="<?php echo $com_id ?>">
                   <!-- รหัสพนักงาน -->
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2">เลขที่ใบงาน :</label>
                    <label class="col-md-4"><?php echo $bk_application_no ?> </label>
                    <label class="col-md-2">Package {{$bk_package}}:</label>
                    <label class="col-md-4"><?php echo $package_detail." = ".$price_minimum."บาท @".$package_price ?> </label>
                    <label class="col-md-2">วันที่ใช้งาน</label>
                    <label class="col-md-10"><?php echo $date_start.' '.$time_start ?> ถึง <?php echo $date_end.' '.$time_end ?></label>
                  </div>

                  <!-- วันทีเริ่มงาน -->
                  <div class="form-group row">
                    <label for="bk_service_end_date" class="col-md-2 col-form-label">วันที่ยกเลิกงาน <label class="text-red">*</label></label>
                    <div class="col-md-4" id="groupbk_start_close_date">
                      <input id="bk_service_end_date" name="bk_service_end_date" type="text" class="form-control bkDate" value="{{$date_start}}">
                      <div hidden="true" id="fbbk_start_close_date" class="form-control-feedback"></div>
                    </div>
                    <label for="bk_service_end_time" class="col-md-2 col-form-label">เวลายกเลิกงาน <label class="text-red">*</label></label>
                    <div class="col-md-4"  id="groupbk_start_close_time">
                      <input id="bk_service_end_time" name="bk_service_end_time" type="text" class="form-control bkTime" value="{{$time_start}}">
                      <div hidden="true" id="fbbk_start_close_time" class="form-control-feedback"></div>
                    </div>
                  </div><!-- วันทีเริ่มงาน -->

                  <div class="form-group row">
                    <label for="bk_service_charge" class="col-md-2 col-form-label">ค่าดำเนินการ<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_service_charge" name="bk_service_charge" type="text" class="form-control" value="" placeholder="ค่าดำเนินการ" >
                    </div>
                    <label class="text-red col-form-label">* ถ้าไม่มีกรุณาใส่ '0'</label>
                  </div>

                  <div class="form-group row">
                    <label for="bk_service_note" class="col-md-2 col-form-label">หมายเหตุ</label>
                    <div class="col-md-10"  id="groupbk_service_note">
                      <textarea class="form-control"  name="bk_service_note" id="bk_service_note" rows="3"></textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12" align="center">
                      <button type="button" id='btnEjectBooking' class="btn btn-success">ยกเลิกการใช้งาน</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                    </div>
                  </div>
                  </form>
            </div>

            {{-- <div class="modal-footer">
              <button type="submit" class="btn btn-primary" id='saveClose'>บันทึกข้อมูล</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            </div> --}}

    </div>
  </div>
</div>

<script type="text/javascript">

$("#btnEjectBooking").click(function(){
     var bk_id = $("#bk_id").val();
     var emp_id = $("#emp_id").val();
     var car_id = $("#car_id").val();
     var com_id = $("#com_id").val();
     var drive_id = $("#drive_id").val();
     var bk_service_charge = $('#bk_service_charge').val() ;
     var bk_service_end_date= $('#bk_service_end_date').val();
     var bk_service_end_time= $('#bk_service_end_time').val();
     var bk_service_note= $('#bk_service_note').val();
     if (bk_service_charge !='' && bk_service_end_date !='' && bk_service_end_time !='') {
       swal({
         title:"คุณแน่ใจ ?",
         text:"คุณต้องยกเลิกการใช้รถใช่หรือไม่ ",
         type:"warning",
         showCancelButton:true,
         confirmButtonColor:"#2ECC71",
         confirmButtonText:"ใช่",
         cancelButtonText:"ไม่ใช่",
         cancelButtonColor:"#E74C3C",
         closeOnConfirm:false,
       },function(isConfirm){
             $.ajax({url:"/setCloseEjectDB",
                     data:
                     "bk_id="+bk_id+
                     "&emp_id="+emp_id+
                     "&com_id="+com_id+
                     "&bk_service_charge="+bk_service_charge+
                     "&bk_service_end_date="+bk_service_end_date+
                     "&bk_service_end_time="+bk_service_end_time+
                     "&bk_service_note="+bk_service_note,
                     type:"POST",
              success:function(data){
               var obj = JSON.parse(data);
                 if (obj['success']==true) {
                    $.ajax({url:"/ReSetcar",
                    data:"bk_id="+bk_id+"&car_id="+car_id+"&drive_id="+drive_id,
                    type:"POST",
                    success:function(data){
                      var obj = JSON.parse(data);
                          swal({
                            type:"success",
                            title:"สำเร็จ",
                            // text:"คุณทำการบันทึกการจัดรถสำเร็จ",
                            // confirmButtonText:"ตกลง",
                            // confirmButtonColor:"#2ECC71",
                            // closeOnConfirm:true,
                            text: "ระบบกำลังส่ง Email แจ้งเตือน กรุณารอสักครู่",
                            timer: 10000,
                            showConfirmButton: false
                          },function(isConfirm){
                            window.location ="{{ url("/bookingclose") }}";
                          });

                        $.ajax({type:"POST",
                                 data:"id="+$('#bk_id').val(),
                                 url:"/sendcloseeject",
                                 success:function(data){
                                 }
                               })
                    }//function ajax setCloseEjectDB
                  })//ReSetcar
                }//if obj['success']==true
              }//function ajax ReApprove
            });//ReApprove
        })//swal คุณต้องการจัดรถใหม่ใช่หรือไม่
     }else{
       swal({
         type:"error",
         title:"ผิดพลาด",
         text:"กรุณากรอกข้อมูลให้ครบ",
         confirmButtonText:"ตกลง",
         confirmButtonColor:"#2ECC71",
       })
     }
  })//end function

</script>
