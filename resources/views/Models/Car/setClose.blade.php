<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
      <?php
      date_default_timezone_set("Asia/Bangkok");
      $sqlbooking_sql = DB::table('tb_booking')
                    ->leftJoin("tb_car_type",function($join){
                          $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                          $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                      })
                    ->leftJoin("tb_car",function($join){
                          $join->on("tb_booking.car_id","=","tb_car.car_id");
                          $join->on("tb_booking.com_id","=","tb_car.com_id");
                      })
                    ->leftJoin("tb_package",function($join){
                          $join->on("tb_booking.ctype_id","=","tb_package.ctype_id");
                          $join->on("tb_booking.bk_package","=","tb_package.package_type");
                          $join->on("tb_booking.com_id","=","tb_package.com_id");
                      })
                    ->leftJoin("tb_brand",function($join){
                          $join->on("tb_car.brand_id","=","tb_brand.brand_id");
                      })
                    ->leftJoin('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                    ->where('tb_booking.bk_id', '=', $bk_id)
                    ->where('tb_booking.com_id','=', $com_id)
                    ->get();
                    foreach ($sqlbooking_sql as $bk) {
                      $ctype= $bk->ctype_name;
                      $brand= $bk->brand_name;
                      $car_model= $bk->car_model;
                      $car_number= $bk->car_number;
                      $bk_start_start = $bk->bk_start_start;
                      $bk_end_start = $bk->bk_end_start;
                      $bk_application_no = $bk->bk_application_no;
                      $bk_package = $bk->bk_package;
                      $package_price = $bk->package_price;
                      $car_id = $bk->car_id;
                      $drive_id = $bk->drive_id;
                    }
                    $datetime = new DateTime($bk_start_start);
                    $date = $datetime->format('Y/m/d');
                    $time = $datetime->format('H:i');
      ?>
            <div class="modal-header">
                <h5 class="modal-title fa fa-plus text-black"> บันทึกวันเวลาจบงาน {{$bk_id}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form id="frm_close" method="post">
                 <div class="form-group row">
                    <!-- รหัสพนักงาน -->
                     <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
                     <input type="hidden" name="bk_id" id="bk_id" value="<?php echo $bk_id ?>">
                     <input type="hidden" name="car_id" id="car_id" value="<?php echo $car_id ?>">
                     <input type="hidden" name="drive_id" id="drive_id" value="<?php echo $drive_id ?>">
                     <input type="hidden" name="com_id" id="com_id" value="<?php echo $com_id ?>">
                   <!-- รหัสพนักงาน -->
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2">เลขที่ใบงาน :</label>
                    <label class="col-md-4"><?php echo $bk_application_no ?> </label>
                    <label class="col-md-2">Package {{$bk_package}}:</label>
                    <label class="col-md-4"><?php echo $package_price ?> </label>
                  </div>
                  <div class="form-group row"><!-- วันทีเริ่มงาน -->
                    <label for="bk_start_close_date" class="col-md-2 col-form-label">วันที่เริ่มงาน </label>
                    <div class="col-md-4" id="groupbk_start_close_date">
                      <input id="bk_start_close_date" name="bk_start_close_date" type="text" class="form-control bkDate" value="{{$date}}">
                      <div hidden="true" id="fbbk_start_close_date" class="form-control-feedback"></div>
                    </div>
                    <label for="bk_start_close_time" class="col-md-2 col-form-label">เวลาเริ่มงาน </label>
                    <div class="col-md-4"  id="groupbk_start_close_time">
                      <input id="bk_start_close_time" name="bk_start_close_time" type="text" class="form-control bkTime" value="{{$time}}">
                      <div hidden="true" id="fbbk_start_close_time" class="form-control-feedback"></div>
                    </div>
                  </div><!-- วันทีเริ่มงาน -->

                  <div class="form-group row"><!-- วันที่สิ้นสุดงาน -->
                    <label for="bk_end_close_date" class="col-md-2 col-form-label">วันที่สิ้นสุดงาน </label>
                    <div class="col-md-4" id="groupbk_end_close_date">
                      <input id="bk_end_close_date" name="bk_end_close_date" type="text" class="form-control bkDate" value="{{date("Y/m/d")}}">
                      <div hidden="true" id="fbbk_end_close_date" class="form-control-feedback"></div>
                    </div>
                    <label for="bk_close_time" class="col-md-2 col-form-label">เวลาสิ้นสุดงาน </label>
                    <div class="col-md-4"  id="groupbk_close_time">
                      <input id="bk_close_time" name="bk_end_close_time" type="text" class="form-control bkTime" value="{{date("H:i")}}">
                      <div hidden="true" id="fbbk_close_time" class="form-control-feedback"></div>
                    </div>
                  </div><!-- วันที่สิ้นสุดงาน -->

                  <div class="form-group row">
                    <label for="tel" class="col-md-2 col-form-label">เลขไมล์เริ่มงาน<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_mileage_start" name="bk_mileage_start" type="text" class="form-control" value="" placeholder="เลขไมล์เริ่มงาน" >
                    </div>
                    <label for="tel" class="col-md-2 col-form-label">เลขไมล์ปิดงาน<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_mileage_end" name="bk_mileage_end" type="text" class="form-control" value="" placeholder="เลขไมล์ปิดงาน" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="tel" class="col-md-2 col-form-label">ค่าทางด่วน<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_expressway" name="bk_expressway" type="text" class="form-control" value="" placeholder="ค่าทางด่วน" >
                    </div>
                    <label for="tel" class="col-md-2 col-form-label">ค่าจอดรถ<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_parking" name="bk_parking" type="text" class="form-control" value="" placeholder="ค่าจอดรถ" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="bk_gasoline_receipt" class="col-md-2 col-form-label">เลขที่ใบเสร็จค่าน้ำมัน<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_gasoline_receipt" name="bk_gasoline_receipt" type="text" class="form-control" value="" placeholder="เลขที่ใบเสร็จค่าน้ำมัน" >
                    </div>

                    <label for="bk_gasoline" class="col-md-2 col-form-label">ค่าน้ำมัน<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="bk_gasoline" name="bk_gasoline" type="text" class="form-control" value="" placeholder="ค่าน้ำมัน" >
                    </div>
                  </div>

                  <div class="form-group row" style="display:none">
                    <label for="tel" class="col-md-2 col-form-label">ชั่วโมงโอที<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="support_ot" name="support_ot" type="text" class="form-control" value="0" placeholder="ชั่วโมงโอที" >
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="tel" class="col-md-2 col-form-label">ค่าที่พัก<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="support_hotel" name="support_hotel" type="text" class="form-control" value="0" placeholder="ค่าที่พัก" >
                    </div>
                  </div>
                  <label class="form-group row col-md-12 text-red">ช่องที่มี * ถ้าไม่มีกรุณาใส่ '0'</label>
                  <div class="form-group row">
                    <label for="bk_close_note" class="col-md-2 col-form-label">หมายเหตุ</label>
                    <div class="col-md-10"  id="groupbk_close_note">
                      <textarea class="form-control"  name="bk_close_note" id="bk_close_note" rows="3"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="count_person" class="col-md-2 col-form-label">เอกสารแนบ </label>
                    <div class="col-10">
                        <input type="file" id="image" name="image[]" multiple>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12" align="center">
                      <button type="submit" class="btn btn-success">บันทึก</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                    </div>
                  </div>
                  </form>
            </div>

            {{-- <div class="modal-footer">
              <button type="submit" class="btn btn-primary" id='saveClose'>บันทึกข้อมูล</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            </div> --}}

    </div>
  </div>
</div>

<script type="text/javascript">

$("form#frm_close").submit(function(ev){
  ev.preventDefault();
  console.log(this);
    var formData = new FormData(this);
    // for (var value of formData.values()) {
    //          console.log(value);
    //       }
    if (($('#bk_gasoline').val() != '' || $('#bk_gasoline').val() != 0)
        && $('#bk_parking').val() != ''
        && $('#bk_expressway').val() != ''
        && $('#bk_mileage_start').val() != ''
        && $('#bk_mileage_end').val() != ''
        && $('#bk_gasoline_receipt').val() != ''
       )
    {
      console.log("saveClose");
       $.ajax({ type:"POST",
                data :formData,
                url:"/setCloseDB",
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                success:function(data){
                 var obj = JSON.parse(data);
                 if (obj['success']==true) {
                   $.ajax({type:"POST",
                            data:"data="+$('#bk_id').val(),
                            url:"/sendsatisfaction",
                            success:function(data){
                            }
                          })
                   $.ajax({type:"POST",
                            data:"id="+$('#bk_id').val(),
                            url:"/sendclose",
                            success:function(data){
                            }
                          })
                   swal({
                     type:"success",
                     title:"สำเร็จ",
                     // text:"คุณทำการบันทึกการจัดรถสำเร็จ",
                     // confirmButtonText:"ตกลง",
                     // confirmButtonColor:"#2ECC71",
                     // closeOnConfirm:true,
                     text: "ระบบกำลังส่ง Email แจ้งเตือน กรุณารอสักครู่",
                     timer: 10000,
                     showConfirmButton: false
                   },function(isConfirm){
                     window.location ="{{ url("/bookingclose") }}";
                   });

                 }
                 else
                 {
                     addErr(obj['type'],obj['msg'])
                 }
               }
        });
    }else {
      swal({
        type:"error",
        title:"ผิดพลาด",
        text:"กรุณากรอกข้อมูลให้ครบ",
        confirmButtonText:"ตกลง",
        confirmButtonColor:"#2ECC71",
      })
    }
   // console.log("saveClose");
   //  $.ajax({ type:"POST",
   //           data :formData,
   //           url:"/setCloseDB",
   //           async: true,
   //           cache: false,
   //           contentType: false,
   //           processData: false,
   //           success:function(data){
   //            var obj = JSON.parse(data);
   //            if (obj['success']==true) {
   //              $.ajax({type:"POST",
   //                       data:"data="+$('#bk_id').val(),
   //                       url:"/sendsatisfaction",
   //                       success:function(data){
   //                       }
   //                     })
   //              $.ajax({type:"POST",
   //                       data:"id="+$('#bk_id').val(),
   //                       url:"/sendclose",
   //                       success:function(data){
   //                       }
   //                     })
   //              swal({
   //                type:"success",
   //                title:"สำเร็จ",
   //                // text:"คุณทำการบันทึกการจัดรถสำเร็จ",
   //                // confirmButtonText:"ตกลง",
   //                // confirmButtonColor:"#2ECC71",
   //                // closeOnConfirm:true,
   //                text: "ระบบกำลังส่ง Email แจ้งเตือน กรุณารอสักครู่",
   //                timer: 10000,
   //                showConfirmButton: false
   //              },function(isConfirm){
   //                window.location ="{{ url("/bookingclose") }}";
   //              });
   //
   //            }
   //            else
   //            {
   //                addErr(obj['type'],obj['msg'])
   //            }
   //          }
   //   });
  })

  function rmErr(input){
    $("#group"+input.id).removeClass("has-danger");
    $("#group"+input.id+" input").removeClass("form-control-danger");
    $("#fb"+input.id).attr("hidden","hidden");
  }

  function addErr(type,msg){
      $("#group"+type).addClass("has-danger");
      $("#group"+type+" input").addClass("form-control-danger");
      $("#fb"+type).html(msg);
      $("#fb"+type).removeAttr("hidden");
  }

</script>
