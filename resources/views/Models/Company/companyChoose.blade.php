<?php
  $chk_userlogin = DB::table('tb_employee_login')->where(['username'=>$username])->select('com_id')->get();
  $company=array();
  foreach ($chk_userlogin as $com):
    array_push($company,$com->com_id);
  endforeach;
?>
<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >  <div class="modal-header">
          <h5 class="modal-title"> เลือกบริษัทของคุณ</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-backdrop="static" data-keyboard="false">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12" align="center">
            <?php
            $sql_company = DB::table('tb_company')->whereIn('com_id',$company)->get();
            ?>
            @foreach ($sql_company as $data)
              <button type="button" class="btn btn-success btn-sm btn-company" data-id="com_id={{$data->com_id}}">{{$data->com_name}}</button>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$('.btn-company').click(function() {
  var com_id =$(this).data('id');
  var dString = $("#frmLogin").serialize();
  var password = $("#password").val();
  $.ajax({ type:"POST",
           data :dString+"&password="+md5(password)+"&"+com_id,
           url:"/loginCompany",
           success:function(data){
            var obj = JSON.parse(data);
            console.log(obj);
            if (obj['success']==true) {
              if (obj['data']==1) {
                window.location ="{{ url("/firstchange") }}";
              }else if (obj['data']==3) {
                window.location ="{{ url("/SetEmail") }}";
              }
              else {
                window.location ="{{ url("/dashboard") }}";
              }
               // $.ajax({ url:"/locatelogin",
               //       data:latlng+"&user="+user,
               //       type:"GET",
               //       success:function(data){
               //         console.log(data);
               //     }
               //  })
            }
            else
            {
              if (obj['status']=="201") {
                $.ajax({
                    url:"/companyChoose",
                    data :dString+"&password="+md5(password),
                    type:"GET",
                    success:function(data){
                        $(".modal-area").html(data);
                        $("#modalBk").modal("show");
                    }
                  })
              }else {
                if (obj['type'] =='error') {
                  alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')
                }
                else {
                  addErr(obj['type'],obj['msg'])
                }
              }
            }

          }
   });

})
</script>
