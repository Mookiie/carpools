<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลเชื้อเพลิง</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
           <!-- รหัสพนักงาน -->
            <?php
            // dep_id
            $sqlfuel = DB::table('tb_fuel')->orderBy('fuel_id','decs')->limit(1)->get();
            foreach ($sqlfuel as $fuel):
             $id = $fuel->fuel_id;
            endforeach;
             ?>
            <!-- รหัสเชื้อเพลิง -->
              <input type="hidden"  id="color_id" name="color_id" value="<?php echo $id+1 ?>">
          </div><!-- รหัสเชื้อเพลิง -->

          <div class="form-group row"><!-- ชื่อเชื้อเพลิง -->
            <label for="fuel_name" class ="col-md-4 col-form-label text-black">ชื่อเชื้อเพลิง</label>
            <input type="text" class="form-control col-md-6" id="fuel_name" name="fuel_name" placeholder="ชื่อเชื้อเพลิง">
          </div><!-- ชื่อเชื้อเพลิง -->

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#dep_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    createfuel();
 }
});
$(".btn-create").click(function(){
  createfuel();
});

function createfuel(){
var form_create= $("#create-form").serialize();
 $.ajax({
   url:"/createfuel",
   data:form_create,
   type:"POST",
   success:function(data){
      var obj =JSON.parse(data);
    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลประเภทรถยนต์สำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/otheradd";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
   }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
