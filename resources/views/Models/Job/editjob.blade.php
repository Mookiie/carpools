<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลตำแหน่งงาน</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
           <!-- รหัสพนักงาน -->

           <?php $sqljob = DB::table('tb_job')->where("job_id", "=" ,$job_id)->get();
           foreach ($sqljob as $job):
            $job_name = $job->job_name;
           endforeach; ?>

          <!-- รหัสตำแหน่ง -->
           <label for="job_id" class ="col-md-4 col-form-label text-black">รหัสหน่วยงาน</label>
               <input type="text"  id="job_id" name="job_id" value="<?php echo $job_id ?>" readonly>
           </div><!-- รหัสตำแหน่ง -->


          <div class="form-group row"><!-- ชื่อตำแหน่ง -->
            <label for="job_name" class ="col-md-4 col-form-label text-black">ชื่อตำแหน่ง</label>
            <input type="text" class="form-control col-md-6" id="job_name" name="job_name" value="<?php echo $job_name ?>">
          </div><!-- ชื่อตำแหน่ง -->

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#dep_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    Editjob();
 }
});
$(".btn-create").click(function(){
  Editjob();
});

function Editjob(){
var form_create= $("#create-form").serialize();
console.log(form_create);
 $.ajax({
  url:"/updatejob",
  data:form_create,
  type:"POST",
  success:function(data){
      var obj =JSON.parse(data);

    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลตำแหน่งงานสำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/editjob";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
  }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
