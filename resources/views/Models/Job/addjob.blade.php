<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลตำแหน่งงาน</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
           <!-- รหัสพนักงาน -->
            <?php
            $sqlemp = DB::table('tb_employee')->where('emp_id','=',$emp_id)->get();
            foreach ($sqlemp as $emp) {
              $com_id = $emp->com_id;
            }

            // dep_id
            $sqljob = DB::table('tb_job')->where('com_id','=',$com_id)->orderBy('job_id','decs')->limit(1)->get();
            foreach ($sqljob as $job):
             $id = $job->job_id;
            endforeach;
             ?>
             <!-- รหัสบริษัท -->
              <input type="hidden" name="com_id" id="com_id" value="{{$com_id}}">
            <!-- รหัสบริษัท -->

            <!-- รหัสตำแหน่ง -->
              <input type="hidden"  id="job_id" name="job_id" value="<?php echo $id+1 ?>">
          </div><!-- รหัสตำแหน่ง -->

          <!-- แผนก -->
          <div class="form-group row">
              <label for="ctype" class="col-md-4 col-form-label">แผนก </label>
                <select id="dep_id" name="dep_id" class="form-control col-md-6">
                  <?php
                      $sql_dep = DB::table('tb_department')->where('com_id','=',$com_id)->get();
                    ?>
                    @foreach ($sql_dep as $dep)
                      <option value="{{$dep->dep_id}}">{{$dep->dep_name}}</option>
                    @endforeach

                </select>
          </div>
          <!-- แผนก -->

          <div class="form-group row"><!-- ชื่อตำแหน่ง -->
            <label for="dep_name" class ="col-md-4 col-form-label text-black">ชื่อตำแหน่ง</label>
            <input type="text" class="form-control col-md-6" id="job_name" name="job_name" placeholder="ชื่อตำแหน่งงาน">
          </div><!-- ชื่อตำแหน่ง -->

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#dep_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    createJob();
 }
});
$(".btn-create").click(function(){
  createJob();
});

function createJob(){
var form_create= $("#create-form").serialize();
 $.ajax({
  url:"/createjob",
  data:form_create,
  type:"POST",
  success:function(data){
      var obj =JSON.parse(data);

    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลตำแหน่งงานสำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/otheradd";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
  }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
