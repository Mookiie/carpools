  <div class="modal fade" id="modalregister">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">สมัครเข้าใช้งาน</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <label for="emp_fname" class="col-md-3 col-form-label">ชื่อ  </label>
            <div class="col-md-8">
              <input id="emp_fname" name="emp_fname" type="text" class="form-control"  placeholder="ชื่อ" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            </div>
          </div>

            <div class="form-group row">
            <label for="emp_fname" class="col-md-3 col-form-label">นามสกุล  </label>
            <div class="col-md-8">
              <input id="emp_lname" name="emp_lname" type="text" class="form-control" placeholder="นามสกุล" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            </div>
          </div>

          <div class="form-group row">
            <label for="emp_sex" class="col-md-3 col-form-label">เพศ</label>
            <div class="col-8">
            <select class="form-control mr-sm-2"  id="emp_sex" name="emp_sex">

              <option value="1">ชาย</option>
              <option value="2">หญิง</option>
            </select>
          </div>
          </div>

          <div class="form-group row">
            <label for="emp_email" class="col-md-3 col-form-label">อีเมล์</label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="emp_email" name="emp_email" placeholder="you@example.com" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                <div hidden="true" id="fbemp_email" class="form-control-feedback"><br></div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
