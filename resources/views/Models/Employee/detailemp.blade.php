<?php
$sqlemployee = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.emp_id')->where('tb_employee.emp_id', '=', $id)->get();
                foreach ($sqlemployee as $emp):
                  $emp_id=$emp->emp_id;
                  $com_id = $emp->com_id;
                  $emp_fname=$emp->emp_fname;
                  $emp_lname=$emp->emp_lname;
                  $emp_sex=$emp->emp_sex;
                  $job_id=$emp->job_id;
                  $emp_email=$emp->emp_email;
                  $emp_lv = $emp->emp_level;
                  $emp_tel=$emp->emp_tel;
                  $dep_id=$emp->dep_id;
                  $pic = $emp->emp_img;
                  $create_by=$emp->create_by;
                  $update_by=$emp->update_by;
                endforeach;
?>
<div class="modal fade" id="modalemp">
  <div class="modal-dialog" role="document">
  <div class="modal-content">

      <div class="modal-header">
      <h5 class="modal-title  "><?php echo $emp_fname." ".$emp_lname; ?></h5>
      <div align="right">
      </div>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>

      <div class="modal-body" id="tab1">

        <div align="center">
        <img src="image/profile/<?php echo $pic; ?>">
        </div>

        {{-- <div class="row">
        <label class="col-md-4"><b>รหัสพนักงาน</b></label>
          <div class="col-md-8"> --}}
            <?php //echo $id; ?>
          {{-- </div>
        </div> --}}

        <div class="row">
        <label class="col-md-4"><b>ชื่อ-สกุล</b></label>
          <div class="col-md-8">
            <?php echo $emp_fname." ".$emp_lname; ?>
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>เบอร์โทรศัพท์</b></label>
          <div class="col-md-8">
            <?php
              if ($emp_tel <> '') {echo $emp_tel;}
              else { echo '-';}
              ?>
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>Email</b></label>
          <div class="col-md-8">
            <?php echo $emp_email; ?>
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>เพศ</b></label>
          <div class="col-md-8">
            <?php
              if ($emp_sex=="1") {
                echo "ชาย";
              }
              else {
                echo "หญิง";
              }
             ?>
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>แผนก</b></label>
          <div class="col-md-8">
            <?php
            $sql=DB::table('tb_department')->where('dep_id','=',$dep_id)->where('com_id','=',$com_id)->get();
            foreach ($sql as $dep):
              echo $dep->dep_name;
            endforeach;
             ?>
          </div>
        </div>
        <?php
        $sql=DB::table('tb_job')->where('job_id','=',$job_id)->where('com_id','=',$com_id)->get();
        foreach ($sql as $job):
        $job_name =  $job->job_name;
        endforeach;
        if (count($sql)>0) {?>
        <div class="row">
        <label class="col-md-4"><b>ธุรกิจ</b></label>
          <div class="col-md-8">
            {{$job_name}}
          </div>
        </div>
        <?php } ?>
        <div class="row">
        <label class="col-md-4"><b>บันทึกโดย</b></label>
          <div class="col-md-8">
            <?php
            $sql=DB::table('tb_employee')->where('emp_id','=',$create_by)->get();
            foreach ($sql as $emp):
              echo $emp->emp_fname." ".$emp->emp_lname;
            endforeach;
             ?>
          </div>
        </div>

        <?php if($update_by!=""){ ?>
        <div class="row">
        <label class="col-md-4"><b>แก้ไขโดย</b></label>
          <div class="col-md-8">
            <?php
            $sql=DB::table('tb_employee')->where('emp_id','=',$update_by)->get();
            foreach ($sql as $emp):
              echo $emp->emp_fname." ".$emp->emp_lname;
            endforeach;
             ?>
          </div>
        </div>
      <?php } ?>

      <?php if ($emp_id !="") {?>
        <div class="row">
          <div class="col-md-12" align="center">
            <button type="button" class="btn btn-danger btn-sm" id="btn-dislv" data-id="emp=<?php echo $emp_id; ?>">ยกเลิกการใช้งาน</button>
            <button type="button" class="btn btn-warning btn-sm" id="btn-editemp" data-id="emp=<?php echo $emp->emp_id;?>">แก้ไข</button>
          </div>
        </div>
      <?php } ?>


    </div>
    <?php $emp_update = session()->get('user'); ?>
    <div class="modal-body" id="tab2">

        <div align="center">
        <img width="30%" src="image/profile/<?php echo $pic; ?>">
        </div>
    <form id="frm_EmpEdit">
      <input id="emp_update" name="emp_update" type="hidden" class="form-control" value="<?php echo $emp_update; ?>" readonly>

        <div class="row">
        <label class="col-md-4"><b>รหัสพนักงาน</b></label>
          <div class="col-md-8">
            <input id="emp_id" name="emp_id" type="text" class="form-control" value="<?php echo $emp_id; ?>" readonly>
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>ชื่อ</b></label>
          <div class="col-md-8">
            <input id="emp_fname" name="emp_fname" type="text" class="form-control" value="<?php echo $emp_fname; ?>">
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>นามสกุล</b></label>
          <div class="col-md-8">
            <input id="emp_lname" name="emp_lname" type="text" class="form-control" value="<?php echo $emp_lname; ?>">
          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>เบอร์โทรศัพท์</b></label>
          <div class="col-md-8">
            <input id="mtel" name="mtel" type="text" class="form-control" value="<?php echo $emp_tel; ?>" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            <div hidden="true" id="fbmtel" class="form-control-feedback"><br></div>

          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>Email</b></label>
          <div class="col-md-8">
            <input type="email" class="form-control" id="emp_email" name="emp_email" value="<?php echo $emp_email; ?>" onfocus="rmErr(this);" onkeypress="rmErr(this);" readonly>
              <div hidden="true" id="fbemp_email" class="form-control-feedback"><br></div>

          </div>
        </div>

        <div class="row">
        <label class="col-md-4"><b>เพศ</b></label>
          <div class="col-md-8">
            <select class="form-control mr-sm-2"  id="emp_sex" name="emp_sex">
              <!-- <option value="1" selected>ชาย</option>
                    <option value="2">หญิง</option>
                   </select> -->
            <?php
              if ($emp_sex=="1") {
                echo "<option value='1' selected>ชาย</option> ";
                echo "<option value='2'>หญิง</option>";
              }
              else {
                echo "<option value='1'>ชาย</option> ";
                echo "<option value='2' selected>หญิง</option>";
              }
             ?>
             </select>
          </div>
        </div>

        <input type="hidden" class="form-control" id="com_id" name="com_id"  value="{{$com_id}}">

        <div class="row">
          <label for="dep_id" class="col-md-4">ตำแหน่ง</label>
          <div class="col-md-8">
            <select class="form-control mr-sm-2"  id="dep_id" name="dep_id">
            </select>
          </div>
        </div>

        <div class="row">
          <label for="job_id" class="col-md-4">สังกัด/แผนก</label>
          <div class="col-md-8">
              <select class="form-control mr-sm-2"  id="job_id" name="job_id">
              </select>
            </div>
        </div>

        <div class="row">
          <label for="emp_level" class="col-md-4 col-form-label">สิทธิการใช้งาน</label>
          <div class="col-8">
          <select class="form-control mr-sm-2"  id="emp_level" name="emp_level">
            <?php
            $sql=DB::table('tb_level')->where('lv_id','<','100')->get();
            foreach ($sql as $lv):
              if ($lv->lv_id == $emp_lv) {
                echo "<option value='".$lv->lv_id."' selected>".$lv->lv_name."</option>";
              }
              else {
                echo "<option value='".$lv->lv_id."'>".$lv->lv_name."</option>";
              }
            endforeach;
            ?>
          </select>
          </div>
        </div>

        <div class="row">
          <label for="emp_pass" class="col-md-4 col-form-label">Password ยืนยัน</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" id="emp_pass" name="emp_pass" placeholder="กรุณากรอกรหัสผ่านเพื่อยืนยันการบันทึกข้อมูล" onfocus="rmErr(this);" onkeypress="rmErr(this);">
              <div hidden="true" id="fbemp_pass" class="form-control-feedback"><br></div>
          </div>
        </div>

        <div class="row">

          <div class="col-md-12" align="center">
            <button type="button" class="btn btn-success btn-sm" id="btn-save" data-id="emp=<?php echo $emp->emp_id; ?>">บันทึก</button>
          </div>
        </div>
  </form>
      </div>
</div>
</div>
</div>

                <script>
                $(document).ready(function () {
                  var com_id = $("#com_id").val();
                  comtodep(com_id);
                })

                function comtodep(id) {
                  $.ajax({
                    url:"/comtodep",
                    type:"GET",
                    data:"com_id="+id,
                    contentType: false,
                    processData: false,
                    success:function(data){
                   $("#dep_id").html(data);
                     var dep_id = $("#dep_id").val();
                     deptojob(dep_id,id);
                    }
                  })
                }

                $("#dep_id").change(function () {
                  var dep_id = $("#dep_id").val();
                  var com_id = $("#com_id").val();
                  deptojob(dep_id,com_id);
                })

                function deptojob(id1,id2) {
                  $.ajax({
                    url:"/deptojob",
                    type:"GET",
                    data:"dep_id="+id1+"&com_id="+id2,
                    contentType: false,
                    processData: false,
                    success:function(data){
                   $("#job_id").html(data);
                    }
                  })
                }


                $("#tab2").hide("hide");
                $("#btn-editemp").click(function () {
                  $("#tab1").hide("slow");
                  $("#tab2").show("slow");
                })

                $("#btn-save").click(function () {
                  var data = $("#frm_EmpEdit").serialize();
                  // console.log(data);
                  $.ajax({ url:"/empedit",
                           data:data,
                           type:"GET",
                           success:function(data){
                              var obj = JSON.parse(data);
                              // console.log(obj['msg']);
                              if (obj['success']==true) {
                                swal({
                                          title: "แก้ไขข้อมูลพนักงานสำเร็จ",
                                          type: "success",
                                          showCancelButton: false,
                                          confirmButtonColor: "#2ECC71",
                                          confirmButtonText: "ตกลง",
                                          closeOnConfirm: false,
                                        },
                                          function(isConfirm){
                                            if (isConfirm) {
                                              window.location = "/SetPermission";
                                        }
                                    });
                              }
                              else
                              {
                                addErr(obj['type'],obj['msg'])
                              }

                           }
                         })
                })

                $("#btn-dislv").click(function(){
                    var id = $(this).data("id");
                    swal({
                      title:"คุณแน่ใจ ?",
                      text:"คุณต้องการยกเลิกสิทธิการใช้งานใช่หรือไม่? ",
                      type:"warning",
                      showCancelButton:true,
                      confirmButtonColor:"#2ECC71",
                      confirmButtonText:"ใช่",
                      cancelButtonText:"ไม่",
                      cancelButtonColor:"#E74C3C",
                      closeOnConfirm:false,
                    },function(isConfirm){
                      if(isConfirm){
                        $.ajax({ url:"/dislv",
                                 data:id,
                                 type:"GET",
                                 success:function(data){
                          if(data == 1){
                            swal({
                              type:"success",
                              title:"ยกเลิกสิทธิการใช้งานสำเร็จ",
                              text:"คุณได้ยกเลิกสิทธิการใช้งานสำเร็จ",
                              confirmButtonText:"ตกลง",
                              confirmButtonColor:"#2ECC71",
                              closeOnConfirm:true,
                            },function(isConfirm){
                              location.reload();
                            });
                          }
                        }});
                      }
                    });
                });
                function rmErr(input){
                  $("#group"+input.id).removeClass("has-danger");
                  $("#group"+input.id+" input").removeClass("form-control-danger");
                  $("#fb"+input.id).attr("hidden","hidden");
                }

                function addErr(type,msg){
                    $("#group"+type).addClass("has-danger");
                    $("#group"+type+" input").addClass("form-control-danger");
                    $("#fb"+type).html(msg);
                    $("#fb"+type).removeAttr("hidden");
                }
                </script>
