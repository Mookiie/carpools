<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="{{ asset('/js/Jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('/tether/dist/js/tether.min.js') }}"></script>
    <title></title>
  </head>
  <body>
    <form class="" id="frm_upload" method="post">
      <input type="file" id="image" name="image[]" multiple>
      <button type="submit" id="upload">uploads</button>
    </form>
  </body>
</html>

<script type="text/javascript">
  $("form#frm_upload").submit(function(ev){
    console.log('upload');
    ev.preventDefault();
    var formData = new FormData(this);
    for (var value of formData.values()) {
      console.log(value);
    }
    $.ajax({
      url:"/upload_file",
      data:formData,
      type:"POST",
      async: true,
      cache: false,
      contentType: false,
      processData: false,
      success:function(data){

      }
    })
  })
</script>
