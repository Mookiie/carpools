<!DOCTYPE html>
<?php
$id = session()->get('bk_id');
$com_id = session()->get('com_id');
// $id='BK1906000002';
// $com_id = 'C0001';
$sql = DB::table('tb_booking')
       ->leftJoin("tb_car_type",function($join){
             $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
             $join->on("tb_booking.com_id","=","tb_car_type.com_id");
         })
       ->leftJoin("tb_car",function($join){
             $join->on("tb_booking.car_id","=","tb_car.car_id");
             $join->on("tb_booking.com_id","=","tb_car.com_id");
         })
       ->leftJoin("tb_package",function($join){
             $join->on("tb_booking.ctype_id","=","tb_package.ctype_id");
             $join->on("tb_booking.bk_package","=","tb_package.package_type");
             $join->on("tb_booking.com_id","=","tb_package.com_id");
         })
       ->leftJoin("tb_brand",function($join){
             $join->on("tb_car.brand_id","=","tb_brand.brand_id");
         })
       ->join('tb_company', 'tb_company.com_id', '=' , 'tb_booking.com_id')
       ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
       ->where('tb_booking.bk_id', '=', $id)->where('tb_booking.com_id','=',$com_id)->get();
       foreach ($sql as $bk) {
         $job = $bk->bk_job;
         $fullname = $bk->emp_fname." ".$bk->emp_lname;
         $bk_date = $bk->bk_date;
         $dep_id = $bk->dep_id;
         $bk_start_start = $bk->bk_start_start;
         $bk_end_start = $bk->bk_end_start;
         $bk_percon = $bk->bk_percon;
         $obj = $bk->bk_obj;
         $note = $bk->bk_note;
         $mtel = $bk->bk_mtel;
         $ttel = $bk->bk_ttel;
         $status = $bk->bk_status;
         $bkuse = $bk->bk_use;
         $dep_car = $bk->dep_car;
         $ctype_id = $bk->ctype_id;
         $ctype= $bk->ctype_name;
         $com_id = $bk->com_id;
         $com_name = $bk->com_name;
         $package = $bk->bk_package;
         $cus_id = $bk->cus_id;
         $req_id = $bk->req_id;
         $bk_application_no = $bk->bk_application_no;
         $bk_package = $bk->bk_package;
         $package_price = $bk->package_price;
         $package_mininum = $bk->package_mininum;
         $bk_start_close = $bk->bk_start_close;
         $bk_end_close = $bk->bk_end_close;
         $bk_close_at = $bk->bk_close_at;
         $bk_close_note = $bk->bk_close_note;
         $extra_expressway = $bk->extra_expressway;
         $bk_expressway = $bk->bk_expressway;
         $extra_parking = $bk->extra_parking;
         $bk_parking = $bk->bk_parking;
         $include_gasoline = $bk->include_gasoline;
         $bk_gasoline_receipt = $bk->bk_gasoline_receipt;
         $bk_gasoline = $bk->bk_gasoline;
         $bk_mileage_start = $bk->bk_mileage_start;
         $bk_mileage_end = $bk->bk_mileage_end;
         $support_hotel = $bk->support_hotel;
         $hotel = $bk->hotel;
         $ot = $bk->ot;
         $support_ot = $bk->support_ot;
         $price = $bk->price;
         $extra_percent = $bk->extra_percent;
         $package_no = $bk->package_no;
         $com_id = $bk->com_id;
         $setcar_by = $bk->setcar_by;
         $setcar_date = $bk->setcar_date;
         $car_id = $bk->car_id;
         $driver_id = $bk->drive_id;
         $package_detail = $bk->package_detail;
         $price_minimum = $bk->price_minimum;
         $brand_name = $bk->brand_name;
         $bk_service_charge = $bk->bk_service_charge;
         $bk_service_end = $bk->bk_service_end;
         $bk_service_note = $bk->bk_service_note;
       }
$mileage = 0;
$total_service = 0.00;
$total_bk = 0.00;

if ($hotel) {
  $sql =  DB::table('tb_package_detail')
  ->where('package_no','=',$package_no)
          ->where('com_id','=',$com_id);
  if ($support_hotel == 'on') {
    $sql = $sql->where('include_hotel','=',1);
  }else {
    $sql = $sql->where('include_hotel','=',0);
  }
  $sql =  $sql->get();
  $support_hotel = $price;
}else {
  $support_hotel = "-";
}

$location = DB::table('tb_booking_location')->where('bk_id','=',$id)->get();
$setcar = DB::table('tb_employee')->where('emp_id','=',$setcar_by)->get();
foreach ($setcar as $sc) {
  $setcar_name = $sc->emp_fname." ".$sc->emp_lname;
}
$sqlcar =DB::table('tb_car')->where('car_id','=',$car_id)->where("com_id","=",$com_id)->get();
foreach ($sqlcar as $car) {
  $car_number = $car->car_number;
  $car_model = $car->car_model;
}

if($driver_id != 'D0000000000') {
  $sqldriver=DB::table('tb_driver')->where('drive_id','=',$driver_id)->where("com_id","=",$com_id)->get();
  foreach ($sqldriver as $driver) {
    $name_d = $driver->drive_fname." ".$driver->drive_lname;
    $tel = $driver->drive_tel;
  }
}else {
  $name_d = "ไม่ต้องการคนขับ";
  $tel = "-";
}
$arr = array();
if($status == "wait"){
    array_push($arr,"รอการอนุมัติ","warning","#F39C12");
  }else if($status == "approve"){
    array_push($arr,"รอการจัดรถ","info","#3498DB");
  }else if($status == "success"){
    array_push($arr,"สำเร็จ","success","#2ECC71");
  }else if($status == "complete"){
    array_push($arr,"สำเร็จ","success","#2ECC71");
  }else if($status == "eject"){
    array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
  }else if($status == "ejectcar"){
    array_push($arr,"ยกเลิกการเดินทาง","danger","#E74C3C");
  }else if($status == "nonecar"){
    array_push($arr,"ไม่มีรถ","danger","#E74C3C");
  }


function datetimefull($datetime)
{
    $y = substr($datetime,0,4);
    $m = substr($datetime,5,2);
    $d = substr($datetime,8,2);
    $h = substr($datetime,11,2);
    $i = substr($datetime,14,2);
    return $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
}
function datetimesome($datetime1,$datetime2)
{
  $y1 = substr($datetime1,0,4);
  $m1 = substr($datetime1,5,2);
  $d1 = substr($datetime1,8,2);
  $h1 = substr($datetime1,11,2);
  $i1 = substr($datetime1,14,2);
  $y2 = substr($datetime2,0,4);
  $m2 = substr($datetime2,5,2);
  $d2 = substr($datetime2,8,2);
  $h2 = substr($datetime2,11,2);
  $i2 = substr($datetime2,14,2);
  return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
}

$qr = 'http://chart.apis.google.com/chart?chs=200x200&cht=qr&chl='.$id.'&choe=UTF-8';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Carpool Service</title>
    <link rel="icon" href="../image/Logo.png" type="image/gif" sizes="16x16">
  </head>
  <body>
      <div style=' font-family:Verdana, Geneva, sans-serif;
                    padding:10px; border:solid 3px #FFA835 ;'
           align='center'>
          <div align="center" style="padding: 0.5% 5% 0.5% 5%; color:#fff;
                                background:#FFBF3C; ">
              <img src="http://www.sr-carpoolservice.com/upload/image/Logo.png" width="70">
              <p style="margin:0px;">
                <h3>Carpools Service</h3>
              </p>
           </div>
           <div align='left' style='padding: 2% 10% 2% 10%;'>
               <b>เรียนผู้ใช้บริการ,</b>
               <dd>
                 คำขอของคุณได้ดำเนินการเสร็จสิ้น โดยมีรายละเอียด ดังนี้
               </dd>
               <br>
               <div style=' font-family:Verdana, Geneva, sans-serif;
                             padding:10px; border:solid 1px #FFA835 ;
                             '>
                 <span>รายละเอียดข้อมูลจากระบบ Carpools</span>
                 <table class="table">
                   <tbody>
                     <tr>
                       <td width='40%'>เลขที่ Booking</td>
                       <td>:</td>
                       <td>{{$id}} <button type="text" name="button" style="background:{{$arr[2]}}">{{$arr[0]}}</button></td>
                     </tr>
                     <tr>
                       <td>อ้างอิงเลขที่ใบงานลูกค้า </td>
                       <td>:</td>
                       <td>{{$bk_application_no}}</td>
                     </tr>
                     <tr>
                       <td>ชื่อลูกค้า </td>
                       <td>:</td>
                       <td>{{ $com_name }}</td>
                     </tr>
                     <tr>
                       <td>Package {{$bk_package}}</td>
                       <td>:</td>
                       <td>{{$package_detail." = ".$price_minimum."บาท @".$package_price}}</td>
                     </tr>
                     <tr>
                       <td>วันและเวลาที่เดินทาง</td>
                       <td>:</td>
                       <td>
                         <?php if (!$bkuse) {
                           echo datetimefull($bk_start_start); echo " ถึง "; echo datetimefull($bk_end_start);
                         } else {
                           echo datetimesome($bk_start_start,$bk_end_start);
                         }?>
                       </td>
                     </tr>
                     <tr>
                       <td colspan="3">รายละเอียดการเดินทาง ดังนี้</td>
                     </tr>
                         <?php  $locate = DB::table('tb_booking_location')->where('bk_id','=',$id)->get();
                         foreach ($locate as $lo) {
                           $lo_id =$lo->location_id;
                             echo "<tr>";
                             if ($lo->location_id == "1") {
                               echo "<td>สถานที่เริ่มต้น</td><td>:</td><td>".$lo->location_name."</td><br>";
                             }
                             else {
                               echo "<td>สถานที่ ".($lo->location_id-1)."</td><td>:</td><td> ".$lo->location_name."</td><br>";
                             }
                             echo "</tr>";
                           }
                          ?>
                     <tr>
                       <td colspan="3">รายละเอียดการจัดรถ ดังนี้</td>
                     </tr>
                     <tr>
                       <td width='40%'>คนขับ</td>
                       <td>:</td>
                       <td> {{$name_d}}<br>
                     </tr>
                     <tr>
                       <td>ประเภทรถ</td>
                       <td>:</td>
                       <td> {{$ctype}}<br>
                     </tr>
                     <tr>
                       <td>หมายเลขทะเบียน</td>
                       <td>:</td>
                       <td>{{ $car_number }}<br>
                     </tr>
                     <tr>
                       <td>ยี่ห้อ</td>
                       <td>:</td>
                       <td>{{ $brand_name }}<br>
                     </tr>
                     <tr>
                       <td>รุ่น</td>
                       <td>:</td>
                       <td>{{ $car_model }}<br>
                     </tr>
                   </tbody>
                 </table>
               </div>
               <div style=' font-family:Verdana, Geneva, sans-serif;
                             padding:10px; border:solid 1px #FFA835 ;
                             margin-top:20px'>
               <table class="table">
                   <tbody>
                     <tr>
                       <td colspan="4">รายละเอียดการใช้งาน ดังนี้</td>
                     </tr>
                     <tr>
                       <td width='30%'>เลขที่ JOB</td>
                       <td>:</td>
                       <td colspan="2" rowspan="12" style="color:rgba(255, 0, 0, 0.6);font-size:50px;text-align:center;transform: rotate(-20deg)"> Canceled </td>
                     </tr>
                     <tr>
                       <td>วันที่ใช้งานจริง</td>
                       <td>:</td>
                       <?php
                       $datetime1 = new DateTime($bk_start_start);
                       $datetime2 = new DateTime($bk_end_start);
                       $diff_time = $datetime2->diff($datetime1);
                       $hours_cal = $diff_time->h;
                       $minutes= $diff_time->i;
                       $hours_cal = $hours_cal + ($diff_time->days*24);
                       $hours_cal = ($minutes > 0) ? $hours_cal+1 : $hours_cal;
                       $hours_close = 0;
                       if ($bk_end_close != '') {
                         $datetime1 = new DateTime($bk_end_close);
                         if ($bk_start_close !='') {
                           $datetime2 = new DateTime($bk_start_close);
                         }else {
                           $datetime2 = new DateTime($bk_start_start);
                         }
                         $diff = $datetime2->diff($datetime1);
                         $hours_close = $diff->h;
                         $minutes= $diff->i;
                         $hours_close = $hours_close + ($diff->days*24);
                         $hours_close = ($minutes > 0) ? $hours_close+1 : $hours_close;
                       }
                       $max = max($hours_cal,$hours_close);
                       ?>

                       <td colspan="2"> </td>
                     </tr>
                     <tr>
                       <td>เลขไมล์ที่ใช้งาน</td>
                       <td>:</td>
                       <td colspan="2"> <br>
                     </tr>
                     <tr>
                       <td>ราคาตาม Package {{$bk_package}}</td>
                       <td>:</td>
                       <td width='15%'></td>
                       <td> <br>
                     </tr>
                     <tr>
                       <td>ค่าบริการส่วนต่าง</td>
                       <td>:</td>
                       <td></td>
                       <td><br>
                     </tr>
                     @if ($ot)
                       <?php
                         $total_bk += $support_ot*170;
                        ?>
                       <tr>
                         <td>ค่าโอที (170 thb/hr.)</td>
                         <td>:</td>
                         <td></td>
                         <td><br>
                       </tr>
                     @endif
                     <tr>
                       <td>ค่าบริการ</td>
                       <td>:</td>
                       <td width='20%'></td>
                       <td> <span class="Upperlined bold"></span><br>
                     </tr>
                     @if ($extra_expressway)
                       <?php
                         $total_service =+ $bk_expressway;
                       ?>
                       <tr>
                         <td>ค่าทางด่วน</td>
                         <td>:</td>
                         <td colspan="2"> <br>
                       </tr>
                     @endif
                     @if ($extra_parking)
                       <tr>
                         <td>ค่าที่จอดรถ</td>
                         <td>:</td>
                         <td colspan="2"><br>
                       </tr>
                     @endif
                     @if (!$include_gasoline)
                       <tr>
                         <td>ค่าน้ำมัน</td>
                         <td>:</td>
                         <td colspan="2"> <br>
                       </tr>
                     @endif
                     <tr>
                       <td>ค่าบริการอื่นๆ</td>
                       <td>:</td>
                       <td colspan="2"> <span class="Upperlined bold"></span><br>
                     </tr>
                     <tr>
                       <td>ค่าดำเนินการ {{$extra_percent.'%'}}</td>
                       <td>:</td>
                       <td colspan="2"> <span class="Underlined"></span><br>
                     </tr>
                     <tr>
                       <td>รวมค่าบริการอื่นๆ</td>
                       <td>:</td>
                       <td> <span class="dblUnderlined bold"></span><br>
                       <td> <span class="bold"></span><br>
                     </tr>
                     @if ($hotel)
                       <tr>
                         <td>ค่าที่พัก</td>
                         <td>:</td>
                         <td></td>
                         <td><br>
                       </tr>
                     @endif
                     <tr>
                       <td>รวมค่าบริการทั้งหมด (ไม่รวม vat)</td>
                       <td>:</td>
                       <td><br>
                       <td> <span class="dblUnderlined Upperlined bold"></span><br>
                     </tr>
                     <tr style="color:red;">
                       <td colspan="4">แจ้งยกเลิกเมื่อ : {{ datetimefull($bk_service_end) }}</td>
                     </tr>
                     <tr style="color:red;">
                       <td colspan="4">ค่าบริการดำเนินการ (ยกเลิกการขอใช้บริการ) : {{$bk_service_charge}} บาท</td>
                     </tr>
                     <tr style="color:red;">
                       <td colspan="4">หมายเหตุ : {{$bk_service_note}}</td>
                     </tr>
                   </tbody>
                 </table>
               </div>

           </div>
            {{-- <div>
              <p>QR CODE สำหรับเช็คสถานะและใช้บริการ</p>
                <img style='text-decoration:none;
                background:#2196f3;
                color:#fff;
                border:none;
                transition: .5s;
                cursor:pointer;
                border-bottom: solid 2px #0d47a1;
                padding:20px 20px;border-radius:10px;width:15%;' src={{$qr}}/>
            </div> --}}

        <div align='center' style='padding: 2% 20% 1% 50%;'>
          <p>ขอบคุณที่ใช้บริการ</p>
          <p>ขอแสดงความนับถือ,<br/>Carpool Service</p>
        </div>
        <div align='center'>
        <p><small>หากท่านมีข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อที่ คุณสุรัตน์(090-1978506)</small></p>
        </div>
      </div>
    </div>
  </body>
</html>
