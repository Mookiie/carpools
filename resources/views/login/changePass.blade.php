@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
@foreach ($users as $r)
  <?php
        $emp_id = $r->emp_id;
   ?>
@endforeach
<div class="container-dashboard">
<div class="card col-md-12 row">

  <div class="card-header offset-sm-2 col-md-8">
    <span class="fa fa-pencil-square-o "> เปลี่ยนรหัสผ่าน</span>
  </div>

  <div class="card offset-sm-2 col-md-8">
      <div class="card-block">
        <form id="frmChangePass" >
          <input type="hidden" class="form-control" id="emp_id" name="emp_id" value="{{$emp_id}}">
        <div class="form-group row" id="groupold_pass"><!-- รหัสผ่านเดิม -->
          <label for="old_pass" class ="offset-sm-1 col-md-3 col-form-label " align="right">รหัสผ่านเดิม</label>
          <input type="password" class="form-control col-md-5" id="old_pass" name="old_pass" placeholder="รหัสผ่านเดิม" onfocus="rmErr(this);" onkeypress="rmErr(this);">
          <div hidden="true" id="fbold_pass" class="form-control-feedback"></div>
        </div><!--รหัสผ่านเดิม -->

        <div class="form-group row" id="groupnew_pass"><!-- รหัสผ่านใหม่่ -->
          <label for="new_pass" class ="offset-sm-1 col-md-3 col-form-label " align="right">รหัสผ่านใหม่</label>
          <input type="password" class="form-control col-md-5" id="new_pass" name="new_pass" placeholder="รหัสผ่านใหม่" onfocus="rmErr(this);" onkeypress="rmErr(this);" disabled>
          <div hidden="true" id="fbnew_pass" class="form-control-feedback"></div>
          {{-- <small class="form-text text-muted">Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</small> --}}
        </div><!-- รหัสผ่านใหม่่ -->

        <div class="form-group row" id="groupcon_pass"><!-- ยืนยันรหัสผ่านใหม่ -->
          <label for="con_pass" class ="offset-sm-1 col-md-3 col-form-label " align="right">ยืนยันรหัสผ่านใหม่</label>
          <input type="password" class="form-control col-md-5" id="con_pass" name="con_pass" placeholder="ยืนยันรหัสผ่านใหม่" onfocus="rmErr(this);" onkeypress="rmErr(this);" disabled>
          <div hidden="true" id="fbcon_pass" class="form-control-feedback"></div>
          {{-- <small class="form-text text-muted">Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</small> --}}
        </div><!-- ยืนยันรหัสผ่านใหม -->

        <div class="form-group form-group-row" align="center">
          <button type="button" class="btn btn-success" id="btn-change" disabled>ยืนยัน</button>
          <a href="/dashboard"><button type="button" class="btn btn-danger">ยกเลิก</button></a>
        </div>
        </form>

      </div>
  </div>
<br>
</div>
</div>

<script>

$(document).ready(function(){
  $("#old_pass").focus();
});

$("#old_pass").keyup(function () {
  delDisabled("new_pass");
})

$("#new_pass").keyup(function () {
  delDisabled("con_pass");
})

$("#con_pass").keyup(function () {
  delDisabled("btn-change");
});

$("#frmChangePass").keypress(function(event){
   var kc = event.keyCode;
   if(kc==13){
      $(".btn-change").trigger("click");
   }
 })

$("#btn-change").click(function(){
      change_pass();
});

 function change_pass(){
  var dString = $("#frmChangePass").serialize();
  var valnewpass = checkPassword($("#new_pass").val());
  var valconpass= checkPassword($("#con_pass").val());
  // console.log(dString+"&valnew="+valnewpass+"&valcon="+valconpass);
   $.ajax({ type:"POST",
            data :dString+"&valnew="+valnewpass+"&valcon="+valconpass,
            url:"/changepass",
            success:function(data){
              // console.log(data);
             var obj = JSON.parse(data);
            //  console.log(obj);
            // //  console.log(obj['success']);
            // //  console.log(obj['type']);
            // //  console.log(obj['msg']);
             if (obj['success']==true) {
               swal({
                         title: "เปลี่ยนรหัสผ่านคุณสำเร็จ",
                         text: "กรุณาเข้าระบบใหม่",
                         type: "success",
                         showCancelButton: false,
                         confirmButtonColor: "#2ECC71",
                         confirmButtonText: "ตกลง",
                         closeOnConfirm: false,
                       },
                         function(isConfirm){
                           if (isConfirm) {
                             window.location = "/logout";
                       }
                   });
             }
             else
             {
               addErr(obj['type'],obj['msg'])
             }

           }
    });
 }

 function checkPassword(str)
 {
   // at least one number, one lowercase and one uppercase letter
   // at least six characters that are letters, numbers or the underscore
   var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
   return (str).match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/);
   // return re.test(str);
 }

 function delDisabled(id){
   $("#"+id).removeAttr("disabled");
 }

function rmErr(input){
  $("#group"+input.id).removeClass("has-danger");
  $("#group"+input.id+" input").removeClass("form-control-danger");
  $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
    $("#group"+type).addClass("has-danger");
    $("#group"+type+" input").addClass("form-control-danger");
    $("#fb"+type).html(msg);
    $("#fb"+type).removeAttr("hidden");
}
</script>

@endsection
