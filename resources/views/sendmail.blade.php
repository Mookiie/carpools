
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <?php
          $date1 = date('Y-m-d',strtotime(date("Y-m-d") . "+1 days"));
          $sql_booking1 = DB::table("tb_booking")
                                ->where("bk_start_start","Like", $date1."%")
                                ->where("bk_status","=","success")
                                ->where("send_alert","=",null)
                                ->select("bk_id")->get();
          $text1 = "";
          $count1 = "";
          if (count($sql_booking1)>0) {
            $count1 = count($sql_booking1);
            foreach ($sql_booking1 as $bk) {
                if ($text1 == '') {
                  $text1 .= $bk->bk_id;
                }
                else {
                  $text1 .= ";".$bk->bk_id;
                }
            }
          }

          $date2 = date('Y-m-d',strtotime(date("Y-m-d") . "-1 days"));
          $sql_booking2 = DB::table("tb_booking")
                              ->where("bk_end_start","Like", $date2."%")
                              ->where("bk_status","=","success")
                              ->where("send_satisfaction","=",null)
                              ->select("bk_id")->get();
          $text2 = "";
          $count2 = "";
          if (count($sql_booking2)>0) {
            $count2 = count($sql_booking2);
            foreach ($sql_booking2 as $bk) {
                if ($text2 == '') {
                  $text2 .= $bk->bk_id;
                }
                else {
                  $text2 .= ";".$bk->bk_id;
                }
              }
            }
      ?>
      <input type="text" id="alret_start_bk" name="" value="{{$count1}}">
      <input type="text" id="start_bk"name="start_bk" value="{{$text1}}">
      <input type="text" id="alret_end_bk" name="" value="{{$count2}}">
      <input type="text" id="end_bk" name="end_bk" value="{{$text2}}">

      <?php
            $send_alert = DB::table("tb_booking")
                            ->where("bk_start_start","Like", $date1."%")
                            ->where("bk_status","=","success")
                            ->select("bk_id")->get();
            echo " <br /> send_alert >> ".count($send_alert)." records<br />";
            print_r($send_alert);
            $send_satisfaction = DB::table("tb_booking")
                                ->where("bk_end_start","Like", $date2."%")
                                ->where("bk_status","=","success")
                                ->select("bk_id")->get();
            echo " <br /> send_satisfaction >> ".count($send_satisfaction)." records<br />";
            print_r($send_satisfaction);
      ?>
  </body>
</html>
<script src="{{ asset('/js/Jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('/tether/dist/js/tether.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    if ($('#alret_start_bk').val() >0) {
      $.ajax({type:"POST",
               data:"data="+$('#start_bk').val(),
               url:"/sendalertstart",
               success:function(data){
               }
      })
    }

    if ($('#alret_end_bk').val() >0) {
      $.ajax({type:"POST",
               data:"data="+$('#end_bk').val(),
               url:"/sendsatisfaction",
               success:function(data){
               }
      })
    }
});
</script>
