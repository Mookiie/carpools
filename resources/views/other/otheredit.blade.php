@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $emp_id=$r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
   ?>
@endforeach
<style>
  .inCard{
    margin: 5px auto;
    padding: 2px;
    border:solid 1px #0d47a1;
    border-radius: 0px;
  }
  .text-black{
  color: #000;
  }
  .card-outline-scb{
    background-color:#42a5f5;
  }
  .dash-headder,.dash-headder span{
    color:#000 !important;
    padding: 5px;
    margin-top:15px;
    border-bottom: solid 0px #ccc;
  }

</style>

<div class="container-dashboard">
    <div class="col-md-12 row">
      <input type="hidden"  id="emp_id" value="{{$emp_id}}">
      <input type="hidden"  id="com_id" value="{{$com_id}}">

<?php if ($lv_user==999) { ?>
      <div class="col-md-12">
        <div class="card">
          <div class="dash-headder">
            <span class="fa fa-university" style="color:#000;">&nbsp;&nbsp; รายการข้อมูลบริษัท</span>
          </div>
            <div class="card-block row" >

              <div class="card inCard col-md-10">
                  <div class="card-outline-scb card-block">
                    <h5><small>ข้อมูลบริษัท</small></h5>
                    <?php

                      $com = DB::table('tb_company')->count();

                    ?>
                    <h2>{{ $com }}<small><small><small> รายการ</small></small></small></h2>

                  </div>
                  <div class="card-block text-right">
                    <button class="btn btn-sm btn-outline-primary " id="editcom" > แก้ไขข้อมูล</button>
                  </div>
              </div>

            </div>
          </div>
        </div>
<?php } ?>

        <div class="col-md-12">
          <div class="card" style="margin-top:5px;">
            <div class="dash-headder">
              <span class="fa fa-university" >&nbsp;&nbsp; รายการข้อมูลอื่นๆ</span>
            </div>
              <div class="card-block row" >

                <div class="card inCard col-md-5" >
                    <div class="card-outline-scb card-block">
                      <h5><small>ข้อมูลหน่วยงาน</small></h5>
                      <?php
                        $department = DB::table('tb_department')->where('com_id','=',$com_id)->count();
                      ?>
                      <h2>{{ $department }}<small><small><small> รายการ</small></small></small></h2>

                    </div>

                    <div class="card-block text-right" >
                      <div>
                        <button class="btn btn-sm btn-outline-primary " id="editdep" > แก้ไขข้อมูล</button>
                      </div>


                    </div>
                </div>

                <div class="card inCard col-md-5">
                    <div class="card-outline-scb card-block">
                      <h5><small>ข้อมูลตำแหน่งงาน</small></h5>
                      <?php
                        $job = DB::table('tb_job')->count();
                      ?>
                      <h2>{{ $job }}<small><small><small> รายการ</small></small></small></h2>

                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-primary " id="editjob"> แก้ไขข้อมูล</span></button>
                    </div>
                </div>

              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card" style="margin-top:5px;">
              <div class="dash-headder">
                <span class="fa fa-car" >&nbsp;&nbsp; รายการข้อมูลรถ</span>
              </div>
                <div class="card-block row">

                <div class="card inCard col-md-2">
                    <div class="card-outline-scb card-block">
                      <h5><small>ประเภทรถยนต์</small></h5>
                      <?php $ctype = DB::table('tb_car_type')->count(); ?>
                      <h2>{{$ctype}}<small><small><small> รายการ</small></small></small></h2>

                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-primary " style="margin-right:10px" id="editctype">แก้ไขข้อมูล</span></button>
                    </div>
                </div>

                <div class="card inCard col-md-2">
                    <div class="card-outline-scb card-block">
                      <h5><small>สีรถยนต์</small></h5>
                       <?php $color = DB::table('tb_color')->count(); ?>
                      <h2>{{$color}}<small><small><small> รายการ</small></small></small></h2>
                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-primary " style="margin-right:10px" id="editcolor">แก้ไขข้อมูล</span></button>
                    </div>
                </div>

              <div class="card inCard col-md-2">
                  <div class="card-outline-scb card-block">
                    <h5><small>เชื้อเพลิง</small></h5>
                    <?php $fuel = DB::table('tb_fuel')->count(); ?>
                    <h2>{{$fuel}}<small><small><small> รายการ</small></small></small></h2>

                  </div>
                  <div class="card-block text-right">
                    <button class="btn btn-sm btn-outline-primary " style="margin-right:10px" id="editfuel">แก้ไขข้อมูล</span></button>
                  </div>
              </div>

              <div class="card inCard col-md-2">
                  <div class="card-outline-scb card-block">
                    <h5><small>ยี่ห้อรถยนต์</small></h5>
                     <?php $brand = DB::table('tb_brand')->count(); ?>
                    <h2>{{$brand}}<small><small><small> รายการ</small></small></small></h2>
                  </div>
                  <div class="card-block text-right">
                    <button class="btn btn-sm btn-outline-primary " style="margin-right:10px" id="editbrand">แก้ไขข้อมูล</span></button>
                  </div>
              </div>
            </div>
          </div>
        </div>

    </div>
  </div>
    <div class="modal-area"></div>

    <script type="text/javascript">

    $("#editdep").click(function(){
      window.location = "/editdep";
    });
    //
    $("#editjob").click(function(){
      window.location = "/editjob";
    });

    $("#editcar").click(function(){
      window.location = "/editcar";
    });

    $("#editctype").click(function(){
      window.location = "/editctype";
    });

    $("#editcolor").click(function(){
      window.location = "/editcolor";
    });

    $("#editfuel").click(function(){
      window.location = "/editfuel";
    });

    $("#editbrand").click(function(){
      window.location = "/editbrand";
    });



    </script>

@endsection
