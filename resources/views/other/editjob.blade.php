@extends('welcome')
@include('dashboard.topNavbar')
@include('dashboard.SideNavbar')
@section('content')

@foreach ($users as $r)
  <?php
        $emp_id=$r->emp_id;
        $com_id = $r->com_id;
        $lv_user = $r->emp_level;
   ?>
@endforeach
<div class="container-dashboard">

  <input type="hidden"  id="emp_id" value="{{$emp_id}}">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <span class="fa fa-university">&nbsp;&nbsp; ข้อมูลตำแหน่งงาน</span>
        </div>
          <div class="card-block">

             <table class="table table-bordered tblBk">
                <thead>
                  <th class="text-black" width="20%">รหัสตำแหน่งงาน</th>
                  <th class="text-black">ชื่อตำแหน่งงาน</th>
                  <th class="text-black" width="2%">แก้ไข</th>
                </thead>

                <tbody class="">
                  <?php
                  $job = DB::table('tb_job')->where('com_id','=',$com_id)->get();
                  $numjob = DB::table('tb_job')->where('com_id','=',$com_id)->count();
                  if($numjob > 0){
                  ?>
                    @foreach ($job as $job)
                    <?php
                          if($numjob > 0){
                              ?>
                              <tr>
                                 <td class="text-black">{{$job->job_id}}</td>
                                 <td class="text-black">{{$job->job_name}}</td>
                                 <td class="text-black" align='center'>
                                   <button class="btn btn-sm btn-warning btn-edit" data-id="job={{$job->job_id}}" id="edit">แก้ไข</button>
                                 </td>
                               </tr>
                               <?php }
                                elseif ($numjob = 0){ ?>
                                  <tr>
                                      <td colspan="3" align="center" class="text-black"><h5>ไม่พบข้อมูล</h5></td>
                                  </tr>
                                <?php }?>
                        @endforeach
                  <?php }else{  ?>
                              <tr>
                                  <td colspan="3" align="center" class="text-black"><h5>ไม่พบข้อมูล</h5></td>
                              </tr>
                  <?php } ?>
                </tbody>
              </table>
          </div>
          <div align="center" style="padding-bottom:15px">
            <a href="/otheredit"><button class="btn btn-sm btn-warning">กลับหน้าหลัก</button></a>
          </div>
        </div>
      </div>

</div>
<div class="modal-area"></div>

<script>

$(".btn-edit").click(function(){
  var id =$("#emp_id").val();
  var detail = $(this).data("id");
  // console.log("emp_id="+id+"&"+detail);
  $.ajax({
           url:"/jobedit",
           data:"emp_id="+id+"&"+detail,
           type:"GET",
           success:function(data){
           $(".modal-area").html(data);
           $("#modalBk").modal("show");
         }
     });
});

</script>
@endsection
