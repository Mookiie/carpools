@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
@foreach ($users as $r)
  <?php
        $emp_id=$r->emp_id;
        $lv_user = $r->emp_level;
   ?>
@endforeach
<div class="container-dashboard">
<div class="col-md-12 row">
  <input type="hidden"  id="emp_id" value="{{$emp_id}}">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <span class="fa fa-university" style="color:#000;">&nbsp;&nbsp; ข้อมูลเชื้อเพลิง</span>
        </div>
          <div class="card-block row offset-md-1 col-md-10" >


             <table class="table table-bordered tblBk">
                <thead>
                  <th class="text-black" width="20%">รหัสเชื้อเพลิง</th>
                  <th class="text-black">ชื่อเชื้อเพลิง</th>
                  <th class="text-black" width="2%">แก้ไข</th>
                </thead>

                <tbody class="">
                  <?php
                  $fuel = DB::table('tb_fuel')->get();
                  $numfuel = DB::table('tb_fuel')->count();
                  if($numfuel > 0){
                  ?>
                    @foreach ($fuel as $fu)
                    <?php
                          if($numfuel > 0){
                              ?>
                              <tr>
                                 <td class="text-black">{{$fu->fuel_id}}</td>
                                 <td class="text-black">{{$fu->fuel_name}}</td>
                                 <td class="text-black" align='center'>
                                   <button class="btn btn-sm btn-warning btn-edit" data-id="fuel={{$fu->fuel_id}}" id="btn-edit">แก้ไข</button>
</td>                             </td>
                               </tr>
                               <?php }
                                elseif ($numfuel = 0){ ?>
                                  <tr>
                                      <td colspan="3" align="center" class="text-black"><h5>ไม่พบข้อมูล</h5></td>
                                  </tr>
                                <?php }?>
                        @endforeach
                  <?php }else{  ?>
                              <tr>
                                  <td colspan="3" align="center" class="text-black"><h5>ไม่พบข้อมูล</h5></td>
                              </tr>
                  <?php } ?>
                </tbody>
              </table>
          </div>
          <div align="center" style="padding-bottom:15px">
            <a href="/otheredit"><button class="btn btn-sm btn-warning">กลับหน้าหลัก</button></a>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="modal-area"></div>

<script>

$(".btn-edit").click(function(){
  var id =$("#emp_id").val();
  var detail = $(this).data("id");
  // console.log("emp_id="+id+"&"+detail);
  $.ajax({
           url:"/fueledit",
           data:"emp_id="+id+"&"+detail,
           type:"GET",
           success:function(data){
           $(".modal-area").html(data);
           $("#modalBk").modal("show");
         }
     });
});

</script>
@endsection
